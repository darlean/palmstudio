package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.NoticeItem;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonDialog;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-11.
 */
public class NotifyActivity extends Activity implements View.OnClickListener
{
    ListViewAdapter adapter;
    ArrayList<NoticeItem> noticeList ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(R.string.activity_notify_title) ;

        textView = (TextView) findViewById(R.id.list_empty_tv) ;
        textView.setText(R.string.empty_notify) ;

        findViewById(R.id.btn_back).setOnClickListener(this);

        noticeList = new ArrayList<NoticeItem>();
        Member.getUsage().getNotification(this, this, "recvNotification");
    }

    public void recvNotification(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                if( result )
                {
                    JSONArray notification = _response.getJSONArray("notification");
                    for( int i = 0; i < notification.length(); ++i )
                    {
                        JSONObject jsonObject = notification.getJSONObject(i);

                        noticeList.add(new NoticeItem(i, jsonObject.optString("not_message"),"", jsonObject.optString("not_datetime"), "NOTIFICATION" ));
                    }

                    adapter = new ListViewAdapter(this, noticeList, R.layout.item_main);
                    final Context context = this ;

                    final ListView listView = (ListView) findViewById(R.id.listView);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                            NoticeItem noticeItem = noticeList.get(position);

                            CommonDialog commonDialog = new CommonDialog(context, -1);
                            commonDialog.setCancelable(false);
                            commonDialog.setMessage(noticeItem.getNOTICE_TITLE());
                            commonDialog.show();
                        }
                    });
                }

                if ( noticeList.size() > 0 )
                {
                    TextView textView = (TextView) findViewById(R.id.list_empty_tv) ;
                    textView.setVisibility(View.INVISIBLE);
                }
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }
            Log.i("Notification", _response.toString());
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;

        }
    }
}
