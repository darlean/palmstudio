package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.ListViewItem;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.ImageUtil;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by darlean on 2016. 1. 31..
 */
public class AddressActivity extends Activity implements View.OnClickListener
{
    private String callParentType = "" ;

    // 리스트뷰 선언
    private ListView listview;

    // 데이터를 연결할 Adapter
    ListViewAdapter adapter;

    // 데이터를 담을 자료구조
    ArrayList<ListViewItem> alist;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_address);

        // Dialog 사이즈 조절 하기
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(getWindow().getAttributes());
        params.width = (int) ImageUtil.convertDpToPixel(320, this);
        params.height = (int) ImageUtil.convertDpToPixel(400, this) ;
        getWindow().setAttributes(params);

        listview = (ListView) findViewById(R.id.listView);

        alist = new ArrayList<ListViewItem>();

        findViewById(R.id.btn_search).setOnClickListener(this);
        findViewById(R.id.btn_close).setOnClickListener(this);

        processIntent() ;
    }

    protected void onNewIntent(Intent intent)
    {
        setIntent(intent) ;
        processIntent() ;
    }

    private void processIntent()
    {
        Intent receivedIntent = getIntent() ;
        callParentType = receivedIntent.getExtras().getString("TYPE") ;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_search:
            {
                EditText editText = (EditText) findViewById(R.id.edit_address) ;

                InputMethodManager imm= (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

                Member.getUsage().findAddress(this, Util.getTextOfEditText(this, R.id.edit_address), this, "recvAddressResult");
            }
            break;

            case R.id.btn_close:
                setResult(RESULT_CANCELED) ;
                finish();
                break ;
        }
    }

    public void selectAddress(int position)
    {
        ListViewItem listViewItem = (ListViewItem) adapter.getItem(position);

        Intent intent ;
        if ( callParentType.equals("ORDER") )
        {
            intent = new Intent(getApplicationContext(), PayActivity.class);
        }
        else
        {
            intent = new Intent(getApplicationContext(), JoinActivity.class);
        }
        intent.putExtra("ZIPCODE", listViewItem.getID()) ;
        intent.putExtra("ADDR1", listViewItem.getLabel());
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        setResult(RESULT_OK, intent) ;
        finish();
    }

    public void recvAddressResult(int _statusCode, JSONObject _response)
    {
        if(_response != null)
        {
            try {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                if ( result )
                {
                    JSONArray ja = _response.getJSONArray("address") ;

                    for (int i = 0; i < ja.length(); i++)
                    {
                        JSONObject json = ja.getJSONObject(i);
                        alist.add(new ListViewItem(json.optString("postnew"), json.optString("addr")));
                    }

                    adapter = new ListViewAdapter(this, alist, R.layout.item_address);

                    listview.setAdapter(adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectAddress(position);
                        }
                    });
                }
                else
                {
                    Util.alertDlg(this, reason) ;
                }

            } catch (Exception e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));

                //Util.alertDlg(context, "error", "검색결과가 없습니다.", "확인");
                Util.alertDlg(this, "error", errors.toString(), "확인");
            }

        }else{

            //ajax error, show error code
            //Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
            Util.alertDlg(this, "error", "Error:" + _statusCode, "확인");
        }
    }
}
