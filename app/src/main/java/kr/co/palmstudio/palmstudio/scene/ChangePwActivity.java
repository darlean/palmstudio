package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonToast;

/**
 * Created by Administrator on 2016-01-20.
 */
public class ChangePwActivity extends Activity implements View.OnClickListener
{
    AQuery              m_aq;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pw);

        m_aq = new AQuery(this);

        TextView tv_title = (TextView) findViewById(R.id.tv_title) ;
        tv_title.setText(R.string.activity_change_pw_title) ;

        findViewById(R.id.btn_back).setOnClickListener(this);

        /*m_aq.id(R.id.edit_old_pass).text("1234");
        m_aq.id(R.id.edit_new_pass).text("1234");
        m_aq.id(R.id.edit_new_pass_re).text("1234");*/

        // register listener
        m_aq.id(R.id.btn_confirm).clicked(this, "changePassword");
    }

    public void changePassword(View view)
    {
        String old_pass = m_aq.id(R.id.edit_old_pass).getEditText().getText().toString();;
        String new_pass = m_aq.id(R.id.edit_new_pass).getEditText().getText().toString();
        String new_pass_re = m_aq.id(R.id.edit_new_pass_re).getEditText().getText().toString();

        Member.getUsage().changePassword(this, old_pass, new_pass, new_pass_re, this, "recvchangedPassowrd");
    }

    public void recvchangedPassowrd(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            String reason = null;
            try
            {
                reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                CommonToast toast ;
                toast = new CommonToast(getApplicationContext()) ;
                toast.showToast(reason, Toast.LENGTH_LONG);
                if( result )
                {
                    Intent intent = new Intent(this, MyInfoActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MyInfoActivity.class);
                startActivity(intent);
                finish();

                Member.getUsage().getMyInfo(this, this, "recvMyInfo" );

                break ;
        }
    }
}
