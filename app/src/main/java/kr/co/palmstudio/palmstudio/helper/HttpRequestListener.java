package kr.co.palmstudio.palmstudio.helper;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 * Created by 홍군 on 2016-02-08.
 */
public abstract class HttpRequestListener
{
    protected HttpCallbackHandler     m_handler = null;
    private HttpRequestListener()   {}
    public HttpRequestListener( Context ctx, Object handler, String callback )
    {
        m_handler = new HttpCallbackHandler(ctx, handler, callback);
    }

    protected HttpCallbackHandler getCallbackHandler() { return m_handler; }

    public Context getContext()
    {
        if( m_handler != null )
            return m_handler.getContext();

        return null;
    }

    public void invokeMethod(int _statusCode, JSONObject _response)
    {
        if( m_handler != null )
            m_handler.invokeMethod(_statusCode, _response);
    }

    public abstract void response(int _statusCode, JSONObject _response) throws JSONException;
    public abstract void response_error(int _statusCode, String _response) throws JSONException;
    public abstract void session_expired(JSONObject _response) throws JSONException;
    public abstract void session_remaked(JSONObject _response) throws JSONException;
}