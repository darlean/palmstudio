package kr.co.palmstudio.palmstudio.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.ListViewItem;

/**
 * Created by darlean on 2016. 1. 29..
 */
public class CommonListDialog extends Dialog
{
    ListViewAdapter adapter;
    ArrayList<ListViewItem> list ;
    ListViewItem selectItem ;
    ListView listView ;

    public CommonListDialog(Context context)
    {
        super(context, R.style.FullHeightDialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RelativeLayout root = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.dialog_layout_list, null);
        setContentView(root);

        list = new ArrayList<ListViewItem>();
        list.add(new ListViewItem("0", "test1"));
        list.add(new ListViewItem("1", "test2"));
        list.add(new ListViewItem("2", "test3"));

        adapter = new ListViewAdapter(context, list, R.layout.item_dialog_list);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                selectItem = list.get(position);
                dismiss();
            }
        });
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener)
    {
        listView.setOnItemClickListener(onItemClickListener) ;
    }

    public void setList(ArrayList<ListViewItem> list)
    {
        this.list = list ;
        adapter.setData(list);
    }

    public ArrayList<ListViewItem> getList()
    {
        return this.list ;
    }

    public ListViewItem getSelectItem()
    {
        return selectItem ;
    }
}
