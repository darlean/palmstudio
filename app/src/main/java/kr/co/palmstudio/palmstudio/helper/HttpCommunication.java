package kr.co.palmstudio.palmstudio.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceActivity;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2015-10-13.
 */
public class HttpCommunication
{
    public AsyncCallback<String> basic_callback ;

    public HttpCommunication (final Context context)
    {
        basic_callback = new AsyncCallback<String>()
        {
            @Override
            public void onResult(String result)
            {
                Util.alertDlg(context, "result", result, "확인") ;
            }

            @Override
            public void exceptionOccured(Exception e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));

                Util.alertDlg(context, "error", errors.toString(), "확인") ;
            }

            @Override
            public void cancelled() {
            }
        };
    }

    public String getJsonFromPHP(String... urls)
    {
        StringBuilder jsonHtml = new StringBuilder();
        try
        {
            // 연결 url 설정
            URL url = new URL(urls[0]);
            // 커넥션 객체 생성
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            // 연결되었으면.
            if(conn != null)
            {
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                // 연결되었음 코드가 리턴되면.
                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    for(;;)
                    {
                        // 웹상에 보여지는 텍스트를 라인단위로 읽어 저장.
                        String line = br.readLine();
                        if(line == null) break;
                        // 저장된 텍스트 라인을 jsonHtml에 붙여넣음
                        jsonHtml.append(line + "\n");
                    }
                    br.close();
                }
                conn.disconnect();
            }
        }
        catch(Exception ex)
        {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));

            jsonHtml.append(errors.toString());
        }

        return jsonHtml.toString();
    }

    public String HttpPostData(String phpURL, StringBuffer sendBuffer)
    {
        try {
            //--------------------------
            //   URL 설정하고 접속하기
            //--------------------------
            URL url = new URL(phpURL);       // URL 설정
            HttpURLConnection http = (HttpURLConnection) url.openConnection();   // 접속
            //--------------------------
            //   전송 모드 설정 - 기본적인 설정이다
            //--------------------------
            http.setDefaultUseCaches(false);
            http.setDoInput(true);                         // 서버에서 읽기 모드 지정
            http.setDoOutput(true);                       // 서버로 쓰기 모드 지정
            http.setRequestMethod("POST");         // 전송 방식은 POST

            // 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
            http.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            //--------------------------
            //   서버로 값 전송
            //--------------------------
            OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
            PrintWriter writer = new PrintWriter(outStream);
            writer.write(sendBuffer.toString());
            writer.flush();
            //--------------------------
            //   서버에서 전송받기
            //--------------------------
            InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "UTF-8");
            BufferedReader reader = new BufferedReader(tmp);
            StringBuilder builder = new StringBuilder();
            String str;
            while ((str = reader.readLine()) != null) {       // 서버에서 라인단위로 보내줄 것이므로 라인단위로 읽는다
                builder.append(str + "\n");                     // View에 표시하기 위해 라인 구분자 추가
            }

            return builder.toString();                       // 전송결과를 전역 변수에 저장
            //((TextView)(findViewById(R.id.text_result))).setText(myResult);
            //Toast.makeText(MainActivity.this, "전송 후 결과 받음", 0).show();
        }
        catch (MalformedURLException e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            return errors.toString() ;
        }
        catch (IOException e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            return errors.toString() ;
        }
    }

    public String HttpPostData(String phpURL, ArrayList<NameValuePair> nameValuePair)
    {
        byte[] result = null;
        String str = "";

        BasicHttpParams params = new BasicHttpParams();
        params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_0);
        //HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_0);

        HttpClient client = new DefaultHttpClient(params);
        HttpPost post = new HttpPost(phpURL);// in this case, params[0] is URL
        post.setParams(params);
        //post.s
        //post.setProtocolVersion();
        //RequestBuil

        //client.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_0);
        //post.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_0);
        ProtocolVersion ver = post.getProtocolVersion();
        Header[] headers = post.getAllHeaders();
        try
        {
            post.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));
            HttpResponse response = client.execute(post);

            response.setParams(params);
            ver = response.getProtocolVersion();

            //response.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_0);


            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpURLConnection.HTTP_OK)
            {
                result = EntityUtils.toByteArray(response.getEntity());
                str = new String(result, "UTF-8");
            }
        }
        catch (UnsupportedEncodingException e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            return errors.toString() ;
        }
        catch (Exception e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            return errors.toString() ;
        }

        return str ;
    }

    public Bitmap getImage(String imageURL)
    {
        Bitmap bmImg = null ;

        try
        {
            URL myFileUrl = new URL(imageURL) ;
            HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();

            InputStream is = conn.getInputStream();

            bmImg = BitmapFactory.decodeStream(is);


        }catch(IOException e){
            e.printStackTrace();
        }

        return bmImg ;
    }

    public void getDataFromPhpServer(final String sub_url, final ArrayList<NameValuePair> nameValuePair, AsyncCallback<String> callback)
    {
        if ( nameValuePair.isEmpty() )
            return ;

        // 비동기로 실행될 코드
        Callable<String> callable = new Callable<String>()
        {
            @Override
            public String call() throws Exception
            {
                String url = "http://kayaban2.cafe24.com/login/" + sub_url ;

                return HttpPostData(url, nameValuePair) ;
            }
        };

        if ( callback == null )
            callback = basic_callback ;

        new AsyncExecutor<String>()
                .setCallable(callable)
                .setCallback(callback)
                .execute();
    }
}
