package kr.co.palmstudio.palmstudio.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by darlean on 2016. 1. 29..
 */
public class CommonDialog extends Dialog implements View.OnClickListener
{
    private AQuery mAQ = null;
    private String photo_url = "" ;

    public CommonDialog(Context context, int layout)
    {
        super(context, R.style.FullHeightDialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if ( layout == -1 )
            layout = R.layout.dialog_layout_1;

        RelativeLayout root = (RelativeLayout) LayoutInflater.from(context).inflate(layout, null);
        setContentView(root);

        mAQ = new AQuery(context);

        if ( findViewById(R.id.btn_ok) != null )
        {
            findViewById(R.id.btn_ok).setOnClickListener(this);
        }
    }

    public void setMessage(String txt)
    {
        TextView textView =(TextView) findViewById(R.id.tv_content);
        if( textView == null ) return;

        textView.setText(txt);
    }

    public void setHTMLText(String html)
    {
        WebView webView = (WebView) findViewById(R.id.wv_contents);
        if( webView == null ) return;

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + html, "text/html", "UTF-8", null);
    }

    public void setTilte(String title)
    {
        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(title);
    }

    public void setPhoto(String photo)
    {
        photo_url = photo ;

        ImageView imageView = (ImageView) findViewById(R.id.img_photo) ;
        mAQ.id(imageView).image(photo, false, false, 0, 0);
    }

    public String getPhotoUrl()
    {
        return photo_url ;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                dismiss();
                break ;
        }
    }
}
