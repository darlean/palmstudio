/*
 * Copyright (c) 2014. Jin Kook Seok & RedstoneSolution. All Rights Reserved.
 */

package kr.co.palmstudio.palmstudio.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Process;

import java.io.PrintWriter;
import java.io.StringWriter;

import kr.co.palmstudio.palmstudio.scene.MainActivity;

/**
 * Created by jinkookseok on 14. 11. 25..
 */
public class IHExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final Context   mContext;
    private final MainActivity mActivity;
    private final Class<?>  mStartActivity;

    public IHExceptionHandler(Context ctx, MainActivity act, Class<?> c) {
        mContext = ctx;
        mActivity = act;
        mStartActivity  = c;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);// You can use LogCat too
        String s = stackTrace.toString();

        Intent intent = new Intent(mContext, mStartActivity);
        //you can use this String to know what caused the exception and in which Activity
        intent.putExtra("uncaughtException", "Exception is: " + stackTrace.toString());
        intent.putExtra("stacktrace", s);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
        mActivity.finish();

        //for restarting the Activity
        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}
