package kr.co.palmstudio.palmstudio.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;

/**
 * Created by darlean on 2016. 1. 29..
 */
public class PhotoDialog extends Dialog implements View.OnClickListener
{
    public PhotoDialog(Context context)
    {
        super(context, R.style.FullHeightDialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RelativeLayout root = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.dialog_layout_photo, null);
        setContentView(root);

        findViewById(R.id.btn_take_photo).setOnClickListener(this);
        findViewById(R.id.btn_select_photo).setOnClickListener(this);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_take_photo:
                dismiss();
                break ;

            case R.id.btn_select_photo:
                dismiss();
                break ;
        }
    }
}
