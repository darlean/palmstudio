package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.NoticeItem;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonDialog;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-20.
 */
public class MyInfoActivity extends Activity implements View.OnClickListener
{
    private AQuery      m_aq;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_info);

        m_aq = new AQuery(this);

        TextView tv_title = (TextView) findViewById(R.id.tv_title) ;
        tv_title.setText(R.string.activity_my_info_title) ;

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_change_pw).setOnClickListener(this);
        findViewById(R.id.btn_del_account).setOnClickListener(this);

        // set text

        // register listener
        m_aq.id(R.id.btn_logout).clicked(this, "requestLogout");

        // get my info
        Member.getUsage().getMyInfo(this, this, "recvMyInfo" );
    }

    public void recvMyInfo(int _statusCode, JSONObject _response)
    {
        if ( _response == null )
            return ;

        Log.i("my_info", _response.toString());

        try
        {
            String mb_id = _response.getString("mb_id");
            String mb_hp = _response.getString("mb_hp");

            m_aq.id(R.id.edit_id).text(mb_id);
            m_aq.id(R.id.edit_hp).text(mb_hp);
        }
        catch (Exception e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            //Util.alertDlg(this, errors.toString());
        }
    }

    public void requestLogout(View view)
    {
        Member.getUsage().logout(this, this, "recvLogout" );
    }

    public void recvLogout(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            Member.getUsage().onLogoutResult(this);

            // 로그 아웃
            Member.getInstance().logout(this);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;


            case R.id.btn_change_pw:
                intent = new Intent(getApplicationContext(), ChangePwActivity.class);
                startActivity(intent);
                finish();
                break ;

            case R.id.btn_del_account:
                intent = new Intent(getApplicationContext(), DelAccountActivity.class);
                startActivity(intent);
                finish();
                break ;
        }
    }
}