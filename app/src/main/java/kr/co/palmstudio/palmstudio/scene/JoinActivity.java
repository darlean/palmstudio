package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpRequest;
import kr.co.palmstudio.palmstudio.helper.HttpRequestListener;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.Util;


/**
 * Created by Administrator on 2016-01-19.
 */
public class JoinActivity extends Activity implements View.OnClickListener
{
    EditText            m_editEmail;
    EditText            m_editName;
    EditText            m_editPhoneNumber;
    String              m_strInsertID;
    String              m_strSerial;
    boolean             m_IsCheckedEmail = false;
    boolean             m_IsCheckedPhone = false;
    AQuery              m_aq;

    private CommonToast commonToast ;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        m_aq = new AQuery(this);

        commonToast = new CommonToast(getApplicationContext()) ;

        // set text
        m_aq.id(R.id.tv_title).text(R.string.activity_join_title);
        /*m_aq.id(R.id.edit_id).text("test7");
        m_aq.id(R.id.edit_email).text("test7@test.com");
        m_aq.id(R.id.chk_confirm_to_event).checked(true);
        m_aq.id(R.id.edit_password).text("1234");
        m_aq.id(R.id.edit_password_confirm).text("1234");

        m_aq.id(R.id.edit_phone).text("010-4813-3998");
        m_aq.id(R.id.edit_cert_serial).text("");

        m_aq.id(R.id.edit_zip).text("111222");

        m_aq.id(R.id.edit_addr_1).text("서울시 도봉구 도봉2동 삼환아파트");
        m_aq.id(R.id.edit_addr_2).text("3동 711호");

        m_aq.id(R.id.chk_confirm_to_service).checked(true);
        m_aq.id(R.id.chk_confirm_to_privacy).checked(true);*/

        // register listener
        m_aq.id(R.id.btn_chk_email).clicked(this, "checkEmail");
        m_aq.id(R.id.btn_req_cert).clicked(this, "reqPhoneCert");
        m_aq.id(R.id.btn_cert_confirm).clicked(this, "confirmPhoneCert");
        m_aq.id(R.id.btn_submit).clicked(this, "submit");

        // get
        m_editName = ( EditText )findViewById(R.id.edit_name);
        m_editEmail = ( EditText )findViewById(R.id.edit_email);
        m_editPhoneNumber = ( EditText )findViewById(R.id.edit_phone);


        findViewById(R.id.btn_back).setOnClickListener(this);
        //findViewById(R.id.btn_chk_email).setOnClickListener(this);

        findViewById(R.id.btn_find_address).setOnClickListener(this);

        m_IsCheckedEmail = false;
        m_IsCheckedPhone = false;
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent receivedIntent)
    {
        if (resultCode == RESULT_CANCELED)
            return;

        if (requestCode == C.Preference.ACT_FIND_ADDRESS)
        {
            m_aq.id(R.id.edit_zip).text(receivedIntent.getStringExtra("ZIPCODE")) ;
            m_aq.id(R.id.edit_addr_1).text(receivedIntent.getStringExtra("ADDR1"));
        }
    }

    public void submit(View view)
    {
        Log.i("Join", "submit");

        if( !m_IsCheckedEmail )
        {
            commonToast.showToast("이메일 중복체크를 해주세요", Toast.LENGTH_LONG);
            return ;
        }

        boolean confirm_to_service  = m_aq.id(R.id.chk_confirm_to_service).isChecked();
        boolean confirm_to_privacy  = m_aq.id(R.id.chk_confirm_to_privacy).isChecked();
        boolean confirm_to_event    = m_aq.id(R.id.chk_confirm_to_event).isChecked();

        if( !confirm_to_event )
        {
            commonToast.showToast("이메일 정보 수신 동의를 해주셔야 합니다.", Toast.LENGTH_LONG);
            return ;
        }

        if( !m_IsCheckedPhone )
        {
            commonToast.showToast("휴대폰 인증을 해주세요", Toast.LENGTH_LONG);
            return ;
        }

        if( !confirm_to_service )
        {
            commonToast.showToast("서비스 이용약관에 동의하셔야 합니다.", Toast.LENGTH_LONG);
            return ;
        }

        if( !confirm_to_privacy )
        {
            commonToast.showToast("개인정보 취급방침에 동의하셔야 합니다.", Toast.LENGTH_LONG);
            return ;
        }

        String userid = m_aq.id(R.id.edit_id).getEditText().getText().toString();
        String name = m_aq.id(R.id.edit_name).getEditText().getText().toString();
        String email = m_aq.id(R.id.edit_email).getEditText().getText().toString();

        String phone = m_aq.id(R.id.edit_phone).getEditText().getText().toString();
        String zip = m_aq.id(R.id.edit_zip).getEditText().getText().toString();

        String addr_1 = m_aq.id(R.id.edit_addr_1).getEditText().getText().toString();
        String addr_2 = m_aq.id(R.id.edit_addr_2).getEditText().getText().toString();

        String password = m_aq.id(R.id.edit_password).getEditText().getText().toString();
        String password_re = m_aq.id(R.id.edit_password_confirm).getEditText().getText().toString();

        RequestParams params = new RequestParams();
        if( confirm_to_service )
            params.put("agree", confirm_to_service);

        if( confirm_to_privacy )
            params.put("agree2", confirm_to_privacy);

        if(confirm_to_event)
            params.put("agree2", confirm_to_privacy);

        params.put("mb_id", userid);
        params.put("mb_name", name);
        params.put("mb_password", password);
        params.put("mb_password_re", password_re);
        params.put("mb_email", email);

        params.put("mb_hp", phone);

        params.put("mb_zip", zip );

        params.put("mb_addr1", addr_1);
        params.put("mb_addr2", addr_2);

        Member.getUsage().join( this, params, this, "recvJoin" );
    }

    public void recvJoin(int _statusCode, JSONObject _response)
    {
        if(_response != null)
        {
            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                //successful ajax call, show status code and json content
                //Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                //CommonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                commonToast.showToast(reason, Toast.LENGTH_LONG);
                if( result )
                {
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();

                //ajax error, show error code
                commonToast.makeText(getApplicationContext(), "Error:" + e.toString(), Toast.LENGTH_LONG).show();
            }

        }else{

            //ajax error, show error code
            commonToast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    public void checkEmail(View view)
    {
        Log.i("Join", "chk email");

        Member.getUsage().checkEmail( this, m_editEmail.getText().toString() ,this, "recvChekedEmail" );
    }

    public void recvChekedEmail(int _statusCode, JSONObject _response)
    {
        if(_response != null){

            try {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");
                if( result )
                {
                    m_IsCheckedEmail = true;
                }
                else
                {
                    m_IsCheckedEmail = false;
                }

                //successful ajax call, show status code and json content
                commonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    public void reqPhoneCert(View view)
    {
        Log.i("Join", "reqPhoneCert");

        Member.getUsage().certPhone( this, m_editPhoneNumber.getText().toString() ,this, "recvChekedPhoneCert" );
    }

    public void recvChekedPhoneCert(int _statusCode, JSONObject _response)
    {
        if(_response != null){

            try {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");
                if( result )
                {
                    m_strSerial = _response.getString("serial");
                    m_strInsertID = _response.getString("insert_id");
                }
                else
                {
                    m_strSerial = "";
                    m_strInsertID = "";
                }

                //successful ajax call, show status code and json content
                commonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    public void confirmPhoneCert(View view)
    {
        Log.i("Join", "confirmPhoneCert");

        Member.getUsage().confirmCertPhone( this, m_strSerial, m_strInsertID, m_editPhoneNumber.getText().toString(), this, "recvCinformPhoneCert" );
    }

    public void recvCinformPhoneCert(int _statusCode, JSONObject _response)
    {
        if(_response != null){

            try {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                if( result )
                {
                    findViewById(R.id.btn_req_cert).setEnabled(false);
                    findViewById(R.id.btn_cert_confirm).setEnabled(false);
                    findViewById(R.id.edit_phone).setEnabled(false);
                    findViewById(R.id.edit_cert_serial).setEnabled(false);

                    m_IsCheckedPhone = true;
                }
                else
                {
                    m_IsCheckedPhone = false;
                }


                //successful ajax call, show status code and json content
                commonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
            {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
                break ;

            case R.id.btn_find_address:
            {
                Intent intent = new Intent(getApplicationContext(), AddressActivity.class);
                intent.putExtra("TYPE", "JOIN");
                startActivityForResult(intent, C.Preference.ACT_FIND_ADDRESS);
            }
            break ;
        }

        //키보드 숨기기
        /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);*/
    }
}