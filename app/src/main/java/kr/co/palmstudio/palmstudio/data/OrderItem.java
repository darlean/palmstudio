package kr.co.palmstudio.palmstudio.data;

/**
 * Created by darlean on 2016. 3. 4..
 */
public class OrderItem
{
    private String order_id ;
    private String order_state ;
    private String photo_type ;
    private String photo_count ;
    private String photo_retouch ;
    private String photo_bg = "photo_bg_1" ;
    private String photo_cloth = "" ;

    private String order_name ;
    private String order_hp ;
    private String order_zip ;
    private String order_addr1 ;
    private String order_addr2 ;

    private String order_price ;
    private String delivery_price ;
    private String pay_type ;
    private String order_date ;

    private String upload_img1 ;
    private String upload_img2 ;
    private String upload_img3 ;

    private boolean bLocal_img1 = false ;
    private boolean bLocal_img2 = false ;
    private boolean bLocal_img3 = false ;

    private String msg = "" ;
    private String download_img ;

    public String P_TYPE;
    public String pay_state;
    public String P_VACT_NUM;
    public String P_VACT_DATE;
    public String P_VACT_NAME;
    public String P_VACT_BANK_CODE;

    public OrderItem(String order_id, String order_state, String photo_type, String photo_count, String photo_retouch, String photo_bg, String photo_cloth, String order_name, String order_hp, String order_zip, String order_addr1,
                     String order_addr2, String order_price, String delivery_price, String order_date, String upload_img1, String upload_img2, String upload_img3, String msg, String download_img,
                     String P_TYPE, String pay_state, String P_VACT_NUM, String P_VACT_DATE, String P_VACT_NAME, String P_VACT_BANK_CODE )
    {
        this.order_id = order_id ;
        this.order_state = order_state ;
        this.photo_type = photo_type ;
        this.photo_count = photo_count ;
        this.photo_retouch = photo_retouch ;
        this.order_name = order_name ;
        this.order_hp = order_hp ;
        this.order_zip = order_zip ;
        this.order_addr1 = order_addr1 ;
        this.order_addr2 = order_addr2 ;
        this.order_price = order_price ;
        this.delivery_price = delivery_price ;
        this.order_date = order_date ;
        this.upload_img1 = upload_img1 ;
        this.upload_img2 = upload_img2 ;
        this.upload_img3 = upload_img3 ;
        //this.pay_type = pay_type ;

        this.msg = msg ;
        this.download_img = download_img ;

        this.P_TYPE = P_TYPE ;
        this.pay_state = pay_state ;
        this.P_VACT_NUM = P_VACT_NUM ;
        this.P_VACT_DATE = P_VACT_DATE ;
        this.P_VACT_NAME = P_VACT_NAME ;
        this.P_VACT_BANK_CODE = P_VACT_BANK_CODE ;


        if ( !photo_bg.equals("") && !photo_bg.equals("0") )
        {
            this.photo_bg = photo_bg ;
        }

        if ( !photo_cloth.equals("") && !photo_cloth.equals("0") )
        {
            this.photo_cloth = photo_cloth ;
        }
    }

    public String getOrder_id()
    {
        return this.order_id ;
    }

    public String getOrder_state()
    {
        return this.order_state ;
    }

    public String getPhoto_type()
    {
        return this.photo_type ;
    }

    public String getPhoto_count()
    {
        return this.photo_count ;
    }

    public String getPhoto_retouch()
    {
        return this.photo_retouch ;
    }

    public void setPhoto_bg(String photo_bg)
    {
        this.photo_bg = photo_bg ;
    }

    public String getPhoto_bg()
    {
        return this.photo_bg ;
    }

    public String getPhoto_cloth()
    {
        return this.photo_cloth ;
    }

    public String getOrder_name()
    {
        return this.order_name ;
    }

    public String getOrder_hp()
    {
        return this.order_hp ;
    }

    public String getOrder_zip()
    {
        return this.order_zip ;
    }

    public String getOrder_addr()
    {
        return this.order_addr1 + " " + this.order_addr2 ;
    }

    public String getOrder_price()
    {
        return this.order_price ;
    }

    public String getDelivery_price()
    {
        return this.delivery_price ;
    }

    public String getOrder_date()
    {
        return this.order_date ;
    }

    public String getPay_type()
    {
        return this.pay_type ;
    }

    public String getUpload_img1() { return this.upload_img1; }
    public String getUpload_img2() { return this.upload_img2; }
    public String getUpload_img3() { return this.upload_img3; }

    public void setUpload_img1(String upload_img1) { this.upload_img1 = upload_img1 ; }
    public void setUpload_img2(String upload_img2) { this.upload_img2 = upload_img2 ; }
    public void setUpload_img3(String upload_img3) { this.upload_img3 = upload_img3 ; }

    public void setbLocal_img1(boolean bLocal_img1) { this.bLocal_img1 = bLocal_img1 ; }
    public void setbLocal_img2(boolean bLocal_img2) { this.bLocal_img2 = bLocal_img2 ; }
    public void setbLocal_img3(boolean bLocal_img3) { this.bLocal_img3 = bLocal_img3 ; }

    public boolean getbLocal_img1() { return this.bLocal_img1 ; }
    public boolean getbLocal_img2() { return this.bLocal_img2 ; }
    public boolean getbLocal_img3() { return this.bLocal_img3 ; }

    public String getMsg() { return this.msg ; }
    public String getDownload_img() { return this.download_img ; }
}
