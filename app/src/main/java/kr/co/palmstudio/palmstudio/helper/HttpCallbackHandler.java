package kr.co.palmstudio.palmstudio.helper;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by 홍군 on 2016-02-13.
 */
public class HttpCallbackHandler
{
    private Reference<Context>  m_wCtx;
    private Reference<Object>   m_wHandler;
    private String              m_callback;

    private HttpCallbackHandler()   {}
    public HttpCallbackHandler( Context ctx, Object handler, String callback )
    {
        m_wCtx      = new WeakReference(ctx);
        m_wHandler  = new WeakReference(handler);
        m_callback  = callback;
    }

    public Context getContext() { return this.m_wCtx == null?null:this.m_wCtx.get(); }
    public Object getHandler() { return this.m_wHandler == null?null:this.m_wHandler.get(); }
    public String getCallback() {
        return this.m_callback;
    }

    public void invokeMethod(int _statusCode, JSONObject _response)
    {
        Object handler  = this.getHandler();
        String callback = this.getCallback();

        if(handler != null && callback != null )
        {
            try {
                Class[] arg = new Class[]{int.class,JSONObject.class};
                Method method = handler.getClass().getMethod(callback, arg);
                Object[] params = new Object[]{_statusCode, _response};
                try {
                    method.invoke(handler, params );
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    Log.e("HttpCallbackHandler", e.toString());
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                    Log.e("HttpCallbackHandler", e.toString());
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                Log.e("HttpCallbackHandler", e.toString());
            }
        }
        else
        {
            Log.e("HttpCallbackHandler", "invalid handler : " );
        }
    }
}
