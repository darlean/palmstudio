package kr.co.palmstudio.palmstudio.camera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.define.Receiver;
import kr.co.palmstudio.palmstudio.scene.CameraActivity;
import kr.co.palmstudio.palmstudio.scene.CustomGallery;
import kr.co.palmstudio.palmstudio.utils.Utils;

public class CameraPreviewFragment extends Fragment
        implements View.OnClickListener {

    /**
     * Data section
     */

    public static String FOLDER_NAME = "Palm Studio";

    /**
     * String section
     */
    public static int crop_height = 0;
    public static int crop_width = 0;
    public static int current_camera_id = CustomCamera.CAMERA_BACK;

    public static int x = 0;
    //    public static float bottom_bar = 0;
    public static float top_bar = 0;
    public static boolean IS_BACK_CAMERA_OR_FRONT_CAMERA = true;
    public static boolean IS_PHOTO_MODE_OR_VIDEO_MODE = true;
    public static boolean IS_RECORDING_VIDEO = false;
    private static int minute = 0;
    private static String RECORDED_FILE_PATH = null;
    private static int second = 0;
    /**
     * View section
     */
    private static CustomCameraPreview mCameraPreview;
    private static ImageButton mIbtnTakePhotoOrRecordVideo;
    private static TextView mTvElapseTime;
    /**
     * Others section
     */

    // new Counter that counts 3000 ms with a tick each 1000 ms
    private static CountDownTimer mCdt = new CountDownTimer(24 * 60 * 60 * 1000, 1000) {
        public void onTick(long millisUntilFinished) {
            //update the UI with the new count
            second += 1;

            if (second == 2)
                // Enable again for user clicked to stop recording video
                mIbtnTakePhotoOrRecordVideo.setEnabled(true);

            if (second == 60) {
                minute = minute + 1;

                second = 0;
            }
            if (minute < 10) {
                mTvElapseTime.setText("0" + minute + " : " + second);
            } else
                mTvElapseTime.setText(minute + " : " + second);
        }

        public void onFinish() {
            //start the activity
        }
    };
    private boolean IS_ALREADY_ADDED_CAMERA_REVIEW_INSIDE = false;
    private boolean IS_FIRST_TIME_GO_TO_CAMERA_PREVIEW_PAGE = false;
    /**
     * The others methods
     */


    private Camera.PictureCallback mPhoto = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // Pass True value to indicate choose Photo mode
            File pictureFile = getOutputMediaFile(getActivity(), true);
            if (pictureFile == null)
            {
                Log.i("", "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();

                rotateTakenPicture(pictureFile, pictureFile.getAbsolutePath());

                // After take picture successfully,
                //      - Need refresh Gallery to see new image
                //      - Go to preview page to see taken picture


                // Refresh Gallery
                Utils.addPictureToGallery(getActivity(), pictureFile.getAbsolutePath());

                refreshCameraPreview(getActivity(), current_camera_id);

                // Should remove view parent before add child
                //Utils.removeViewParent(mCameraPreview);
                ((ViewGroup) mCameraPreview.getParent()).removeView(mCameraPreview);

                /**
                 * Add Camera Preview into Layout
                 */
                // Create our Preview view and set it as the content of our activity.
                mCameraPreview = new CustomCameraPreview(getActivity(), CameraActivity.mCamera);
                mFlCameraPreview.addView(mCameraPreview);

                //resetCamera();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
    private FrameLayout mFlCameraPreview;
    private FrameLayout mFlSwitchFrontOrBackCamera;
    private FrameLayout mFlTakePhotoOrRecordVideo;
    private ImageButton mIbtnClose;
    private ImageButton mIbtnSwitchCropMode;
    private ImageButton mIbtnSwitchFullMode;
    private ImageButton mIbtnSwitchFrontOrBackCamera;
    private ImageButton mIbtnSwitchTakePhotoOrRecordVideo;
    private LinearLayout mLlElapseTime;

    public static Fragment newInstance() {
        CameraPreviewFragment fragment = new CameraPreviewFragment();
        return fragment;
    }

    /**
     * Get current camera ID : Back Camera or Front Camera
     */
    public static int switchCurrentCameraID() {
        int current_camera_id = -1;

        if (IS_BACK_CAMERA_OR_FRONT_CAMERA)
        {
            // Currently - Front, switch to Back
            IS_BACK_CAMERA_OR_FRONT_CAMERA = false;

            current_camera_id = CustomCamera.CAMERA_BACK;
        } else {
            // Currently - Back, switch to Front
            IS_BACK_CAMERA_OR_FRONT_CAMERA = true;

            current_camera_id = CustomCamera.CAMERA_FRONT;
        }

        return current_camera_id;
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(Context mContext, boolean mode) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), FOLDER_NAME);

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.i("", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = null;
        if (mode) {
            // Photo mode
            mediaFile = new File(
                    mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            // Video mode
            mediaFile = new File(
                    mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        }

        String FILE_PATH = mediaFile.getAbsolutePath();

        // Set file path into single ton way
        Uri mUri = Uri.parse(FILE_PATH);

        CameraActivity.camera.setFilePath(mUri.toString());

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext,
                new String[]{FILE_PATH}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
        return mediaFile;
    }

    private static void rotateTakenPicture( File pictureFile, String filePath )
    {
        // 전면카메라는 이미지를 안돌리나?
        Matrix matrix = new Matrix();

        if (CameraPreviewFragment.IS_BACK_CAMERA_OR_FRONT_CAMERA) {
            matrix.postRotate(-90);
        }
        else
        {
            matrix.postRotate(90);
        }

        Bitmap bm = BitmapFactory.decodeFile(filePath);

        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pictureFile);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void refreshCameraPreview(Activity activity, int current_camera_id) {
        if (CameraActivity.mCamera != null) {
            // Stop preview
            CameraActivity.mCamera.stopPreview();

            //NB: if you don't release the current camera before switching, you app will crash
            CameraActivity.releaseCamera();

            // New setting for Camera
            // Should initialize new Camera after released
            CameraActivity.mCamera = CameraActivity.getCameraInstance(current_camera_id);

            // Set Camera Display Orientation
            setCameraDisplayOrientation(activity, current_camera_id);

            try {
                // Set preview display to refresh current face, use when change camera mode
                CameraActivity.mCamera.setPreviewDisplay(mCameraPreview.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                CameraActivity.mCamera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // for the Preview - from http://developer.android.com/reference/android/hardware/Camera.html#setDisplayOrientation(int)
    // note, if orientation is locked to landscape this is only called when setting up the activity, and will always have the same orientation
    public static void setCameraDisplayOrientation(Activity activity, int camera_id) {
        CameraActivity.mCameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(camera_id, CameraActivity.mCameraInfo);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (CameraActivity.mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (CameraActivity.mCameraInfo.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {
            result = (CameraActivity.mCameraInfo.orientation - degrees + 360) % 360;
        }

        // On Sony Device, has issue : Black screen was shown on it
        if (CameraActivity.mCamera != null)
            CameraActivity.mCamera.setDisplayOrientation(result);
    }

    /**
     * Basic methods
     */

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back) {
            // Close current activity
            getActivity().finish();
        } else if (view.getId() == R.id.btn_shoot) {
            /**
             * Need check currently user choose Take Photo mode or Record Video mode.
             * Depend on which mode, use Action
             * - Take photo
             * - Record video
             * relatively.
             */
            if (IS_PHOTO_MODE_OR_VIDEO_MODE) {
                /**
                 * Take photo mode
                 */
                // Begin Take picture
                try {
                    CameraActivity.mCamera.autoFocus(new Camera.AutoFocusCallback() {
                        public void onAutoFocus(boolean success, Camera camera)
                        {
                            CameraActivity.mCamera.autoFocus(null);
                            CameraActivity.mCamera.takePicture(null, null, mPhoto);
                            Log.i("auto_focus", String.valueOf(success));
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        /*else if (view.getId() == R.id.ibtn_switch_to_crop_mode) {
            // Set Crop mode
            CustomCamera.camera.setCropModeOrFullMode(true);

            switchCropModeOrFullMode();
        } else if (view.getId() == R.id.ibtn_switch_to_full_mode) {
            // Set Full mode
            CustomCamera.camera.setCropModeOrFullMode(false);

            switchCropModeOrFullMode();
        } else if (view.getId() == R.id.ibtn_switch_take_photo_or_record_video) {
            // Show Take photo or Record video correctly
            switchPhotoOrVideoMode();
        }*/
        else if (view.getId() == R.id.btn_camera_rotation) {
            /**
             * Should reset camera for camera get new setting
             */

            // Switch between Front Camera & Back Camera
            current_camera_id = switchCurrentCameraID();

            refreshCameraPreview(getActivity(), current_camera_id);

            // Should remove view parent before add child
            //Utils.removeViewParent(mCameraPreview);
            ((ViewGroup) mCameraPreview.getParent()).removeView(mCameraPreview);

            /**
             * Add Camera Preview into Layout
             */
            // Create our Preview view and set it as the content of our activity.
            mCameraPreview = new CustomCameraPreview(getActivity(), CameraActivity.mCamera);
            mFlCameraPreview.addView(mCameraPreview);
        }
        else if (view.getId() == R.id.btn_gallery2)
        {
            // Close current activity
            getActivity().finish();

            Intent mIntent = new Intent(getActivity(), CustomGallery.class);
            mIntent.putExtra(
                    Receiver.EXTRAS_ACTION, Receiver.ACTION_CHOSE_SINGLE_FILE);
//                mIntent.putExtra(
//                        Receiver.EXTRAS_CASE_RECEIVER, Receiver.case_camera_in_setting);

            startActivity(mIntent);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Define first time go to this page, reset after destroy thi page
        IS_FIRST_TIME_GO_TO_CAMERA_PREVIEW_PAGE = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = getLayoutInflater(savedInstanceState).inflate(
                R.layout.activity_camera, container, false);

        // Initial views
        initialViews(v);
        initialData();

        // Should remove view parent before add child
        if (IS_ALREADY_ADDED_CAMERA_REVIEW_INSIDE) {
            //Utils.removeViewParent(mCameraPreview);
            ((ViewGroup) mCameraPreview.getParent()).removeView(mCameraPreview);
        }

        /**
         * Add Camera Preview into Layout
         */
        // Create our Preview view and set it as the content of our activity.
        mCameraPreview = new CustomCameraPreview(getActivity(), CameraActivity.mCamera);
        mFlCameraPreview.addView(mCameraPreview);

        // This boolean detail that in the next time include Camera Preview into layout
        // Should remove current Camera Preview before add new one
        IS_ALREADY_ADDED_CAMERA_REVIEW_INSIDE = true;

        // Reset Camera every time go Back to current page from the other pages
        resetCamera();

        // Switch Crop mode | Full mode
        //switchCropModeOrFullMode();

        /**
         * Should show views correctly
         * - First time go to this page with default mode is Photo
         * - After back from Review page with previous mode after chose
         */
        //switchPhotoOrVideoMode();

        /**
         * Check current device has any camera,
         * if size = 2, need show Switch camera button.
         * otherwise, hide Switch camera button.
         */
        if (CameraActivity.mCamera.getNumberOfCameras() == 2)
            // Show switch camera button
            mIbtnSwitchFrontOrBackCamera.setVisibility(View.VISIBLE);
        else
            // Hide switch camera button
            mIbtnSwitchFrontOrBackCamera.setVisibility(View.INVISIBLE);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // Reset
        IS_ALREADY_ADDED_CAMERA_REVIEW_INSIDE = false;

        IS_FIRST_TIME_GO_TO_CAMERA_PREVIEW_PAGE = false;
    }

    /**
     * Initialize methods
     */
    private void initialData() {
        // Set listener
        //mIbtnClose.setOnClickListener(this);
        //mIbtnSwitchCropMode.setOnClickListener(this);
        //mIbtnSwitchFullMode.setOnClickListener(this);
        mIbtnSwitchFrontOrBackCamera.setOnClickListener(this);
        //mIbtnSwitchTakePhotoOrRecordVideo.setOnClickListener(this);
        mIbtnTakePhotoOrRecordVideo.setOnClickListener(this);
    }

    private void initialViews(View v) {
        mFlCameraPreview = (FrameLayout) v.findViewById(R.id.svPreView);
        //mFlSwitchFrontOrBackCamera = (FrameLayout) v.findViewById(R.id.fl_take_photo_or_record_video);
        //mFlTakePhotoOrRecordVideo = (FrameLayout) v.findViewById(R.id.selector_btn_camera_rotation);

        //mIbtnClose = (ImageButton) v.findViewById(R.id.btn_back);
        //mIbtnSwitchCropMode = (ImageButton) v.findViewById(R.id.ibtn_switch_to_crop_mode);
        //mIbtnSwitchFullMode = (ImageButton) v.findViewById(R.id.ibtn_switch_to_full_mode);
        mIbtnSwitchFrontOrBackCamera = (ImageButton) v.findViewById(R.id.btn_camera_rotation);
        /*mIbtnSwitchTakePhotoOrRecordVideo = (ImageButton) v.findViewById(
                R.id.ibtn_switch_take_photo_or_record_video);*/
        mIbtnTakePhotoOrRecordVideo = (ImageButton) v.findViewById(R.id.btn_shoot);
        /*mLlElapseTime = (LinearLayout) v.findViewById(
                R.id.ll_elapse_time);
        mTvElapseTime = (TextView) v.findViewById(
                R.id.tv_elapse_time);*/
    }

    private void resetCamera() {
        if (CameraActivity.mCamera == null) {
//            int current_camera_id = switchCurrentCameraID();
            CameraActivity.mCamera = CameraActivity.getCameraInstance(current_camera_id);
        }
    }

}

