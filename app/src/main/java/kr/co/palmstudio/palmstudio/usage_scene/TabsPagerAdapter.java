package kr.co.palmstudio.palmstudio.usage_scene;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by darlean on 2016. 5. 6..
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.mNumOfTabs = 8;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new IdCardFragment();
            case 1:
                return new PassportFragment();
            case 2:
                return new DriverCardFragment();
            case 3:
                return new IdPhotoFragment();
            case 4:
                return new ResumeFragment();
            case 5:
                return new VisaFragment();
            case 6:
                return new SatFragment();
            case 7:
                return new BusinessCardFragment();
            default:
                return new IdCardFragment();
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
