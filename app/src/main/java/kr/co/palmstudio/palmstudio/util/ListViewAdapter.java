package kr.co.palmstudio.palmstudio.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.data.BgItem;
import kr.co.palmstudio.palmstudio.data.ListViewItem;
import kr.co.palmstudio.palmstudio.data.NoticeItem;
import kr.co.palmstudio.palmstudio.data.OrderItem;

/**
 * Created by darlean on 2016. 2. 7..
 */
public class ListViewAdapter<T> extends BaseAdapter
{
    private LayoutInflater inflater;
    private ArrayList<T> data;
    private int layout;
    private Context mContext ;
    private int old_position = -1 ;
    private View.OnClickListener onClickListener = null ;
    private int selected_img_bg = -1 ;
    private AQuery mAQ = null;

    public ListViewAdapter(Context context, ArrayList<T> data, int layout)
    {
        this.inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data=data;
        this.layout=layout;
        this.mContext=context ;

        mAQ = new AQuery(this.mContext);
    }

    public ListViewAdapter(Context context, ArrayList<T> data, int layout, View.OnClickListener onClickListener)
    {
        this.inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data=data;
        this.layout=layout;
        this.mContext=context ;
        this.onClickListener = onClickListener ;

        mAQ = new AQuery(this.mContext);
    }

    public void setData(ArrayList<T> data)
    {
        this.data = data ;
        notifyDataSetChanged() ;
    }

    public int getOldPosition() { return old_position ; }
    public void setOldPosition(int position) { old_position = position ; }

    public int getSelected_img_bg() { return selected_img_bg ; }

    @Override
    public int getCount(){return data.size();}

    @Override
    public T getItem(int position){return data.get(position);}

    @Override
    public long getItemId(int position){return position;}

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        final ViewHolder holder ;

        if (convertView == null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(layout, parent, false);

            if ( layout == R.layout.item_bg )
            {
                holder.img_bg = (ImageView) convertView.findViewById(R.id.img_bg);
                holder.check_bg = (CheckBox) convertView.findViewById(R.id.check_bg);
            }
            else if ( layout == R.layout.item_notice || layout == R.layout.item_event )
            {
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                //holder.tv_contents = (TextView) convertView.findViewById(R.id.tv_notice_contents);
                holder.wv_contents = (WebView) convertView.findViewById(R.id.wv_notice_contents);
                holder.layout_contents = (RelativeLayout) convertView.findViewById(R.id.contents_layout) ;

                holder.btn_content_open = (ImageButton) convertView.findViewById(R.id.btn_content_open) ;
                holder.btn_content_close = (ImageButton) convertView.findViewById(R.id.btn_content_close) ;
            }
            else if ( layout == R.layout.item_main )
            {
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_notice_title);
                holder.icon_announcement = (ImageView) convertView.findViewById(R.id.icon_announcement);
                holder.icon_event = (ImageView) convertView.findViewById(R.id.icon_event);
            }
            else if ( layout == R.layout.item_order )
            {
                holder.layout_contents = (RelativeLayout) convertView.findViewById(R.id.contents_layout) ;

                holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_total_price = (TextView) convertView.findViewById(R.id.tv_total_price);
                holder.tv_order_name = (TextView) convertView.findViewById(R.id.tv_order_name);
                holder.tv_order_hp = (TextView) convertView.findViewById(R.id.tv_order_hp);
                holder.tv_order_addr = (TextView) convertView.findViewById(R.id.tv_order_addr);
                holder.tv_order_price = (TextView) convertView.findViewById(R.id.tv_order_price);
                holder.tv_delivery_price = (TextView) convertView.findViewById(R.id.tv_delivery_price);
                holder.tv_total_price2 = (TextView) convertView.findViewById(R.id.tv_total_price2);

                holder.btn_img_photo1 = (ImageButton) convertView.findViewById(R.id.img_photo1);
                holder.btn_img_photo2 = (ImageButton) convertView.findViewById(R.id.img_photo2);
                holder.btn_img_photo3 = (ImageButton) convertView.findViewById(R.id.img_photo3);
                holder.btn_img_bg = (ImageButton) convertView.findViewById(R.id.img_bg);
                holder.btn_img_cloth = (ImageButton) convertView.findViewById(R.id.img_cloth);

                holder.tv_change_photo1 = (TextView) convertView.findViewById(R.id.tv_change_photo1);
                holder.tv_change_photo2 = (TextView) convertView.findViewById(R.id.tv_change_photo2);
                holder.tv_change_photo3 = (TextView) convertView.findViewById(R.id.tv_change_photo3);
                holder.tv_change_bg = (TextView) convertView.findViewById(R.id.tv_change_bg);
                holder.tv_change_cloth = (TextView) convertView.findViewById(R.id.tv_change_cloth);
                holder.btn_memo = (ImageButton) convertView.findViewById(R.id.btn_memo);
                holder.layout_download_result = (LinearLayout) convertView.findViewById(R.id.layout_download_result) ;
                holder.layout_msg = (LinearLayout) convertView.findViewById(R.id.layout_msg) ;
                holder.btn_download_result = (Button) convertView.findViewById(R.id.btn_download_result) ;
                holder.btn_upload_img = (Button) convertView.findViewById(R.id.btn_upload_img) ;
                holder.tv_msg = (TextView) convertView.findViewById(R.id.tv_msg) ;
                holder.btn_completed = (ImageButton) convertView.findViewById(R.id.btn_completed) ;

                holder.btn_content_open = (ImageButton) convertView.findViewById(R.id.btn_content_open) ;
                holder.btn_content_close = (ImageButton) convertView.findViewById(R.id.btn_content_close) ;

                holder.tv_pay_type = (TextView) convertView.findViewById(R.id.tv_pay_type);
                holder.tv_pay_state = (TextView) convertView.findViewById(R.id.tv_pay_state);

                holder.tv_pay_account = (TextView) convertView.findViewById(R.id.tv_pay_account);
                holder.tv_pay_date = (TextView) convertView.findViewById(R.id.tv_pay_date);
                holder.tv_pay_owner = (TextView) convertView.findViewById(R.id.tv_pay_owner);
                holder.tv_pay_bank = (TextView) convertView.findViewById(R.id.tv_pay_bank);

                holder.lo_account = (LinearLayout) convertView.findViewById(R.id.lo_account);
                holder.lo_date = (LinearLayout) convertView.findViewById(R.id.lo_date);
                holder.lo_pay_owner = (LinearLayout) convertView.findViewById(R.id.lo_pay_owner);
                holder.lo_bank = (LinearLayout) convertView.findViewById(R.id.lo_bank);

            }
            else if ( layout == R.layout.item_address )
            {
                holder.tv_zip = (TextView) convertView.findViewById(R.id.tv_zip);
                holder.tv_addr = (TextView) convertView.findViewById(R.id.tv_addr);
            }
            else
            {
                holder.tv_item = (TextView) convertView.findViewById(R.id.textView);
            }

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        if ( layout == R.layout.item_bg )
        {
            BgItem bgItem = (BgItem) data.get(position) ;

            holder.img_bg.setImageResource(bgItem.getBgID());

            if ( holder.check_bg.isSelected() )
            {
                holder.check_bg.setChecked(true);
                holder.img_bg.setColorFilter(0x66000000);
            }
            else
            {
                holder.check_bg.setChecked(false);
                holder.img_bg.setColorFilter(0x00000000);
            }

            holder.check_bg.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    toggle_select_item((GridView) parent, position);
                }
            });
        }
        else if ( layout == R.layout.item_notice || layout == R.layout.item_event )
        {
            NoticeItem noticeItem = (NoticeItem) data.get(position) ;

            holder.tv_title.setText(noticeItem.getNOTICE_TITLE()) ;
            holder.tv_date.setText(noticeItem.getNOTICE_DATE()) ;
            holder.wv_contents.getSettings().setLoadWithOverviewMode(true);
            holder.wv_contents.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            holder.wv_contents.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + noticeItem.getNOTICE_CONTENTS(), "text/html", "UTF-8", null);

            holder.btn_content_open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToggleContents(holder);
                }
            });

            holder.btn_content_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToggleContents(holder);
                }
            });
        }
        else if ( layout == R.layout.item_main )
        {
            NoticeItem noticeItem = (NoticeItem) data.get(position) ;

            holder.tv_title.setText(noticeItem.getNOTICE_TITLE()) ;

            if ( noticeItem.getNOTICE_TYPE().equals("event") )
            {
                holder.icon_event.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.icon_announcement.setVisibility(View.VISIBLE);
            }
        }
        else if ( layout == R.layout.item_order )
        {
            OrderItem orderItem = (OrderItem) data.get(position) ;

            String title = orderItem.getPhoto_type() + " (" + orderItem.getPhoto_count() + "장)" ;
            String addr = "(" + orderItem.getOrder_zip() + ") " + orderItem.getOrder_addr() ;

            String total_price = Util.getDecimalFormat(Integer.parseInt(orderItem.getOrder_price()) + Integer.parseInt(orderItem.getDelivery_price())) + "원" ;
            String order_price = Util.getDecimalFormat(orderItem.getOrder_price()) + "원" ;
            String delivery_price = Util.getDecimalFormat(orderItem.getDelivery_price()) + "원" ;

            holder.tv_date.setText(orderItem.getOrder_date()) ;
            holder.tv_title.setText(title) ;
            holder.tv_total_price.setText(total_price) ;
            holder.tv_total_price2.setText(total_price) ;

            holder.tv_order_name.setText(orderItem.getOrder_name()) ;
            holder.tv_order_hp.setText(orderItem.getOrder_hp()) ;
            holder.tv_order_addr.setText(addr) ;

            holder.tv_order_price.setText(order_price) ;
            holder.tv_delivery_price.setText(delivery_price) ;

            if( orderItem.getUpload_img1().length() > 0 )
            {
                if ( orderItem.getbLocal_img1() )
                {
                    Bitmap bitmap = ImageUtil.loadFixedSizeBitmap(orderItem.getUpload_img1(), 200, 200) ;
                    holder.btn_img_photo1.setImageBitmap(bitmap);
                }
                else
                {
                    String upload_img1 = C.BASE_ROOT_URL + orderItem.getUpload_img1() ;
                    mAQ.id(holder.btn_img_photo1).image(upload_img1, false, false, 68, 0);
                }
            }

            if( orderItem.getUpload_img2().length() > 0 )
            {
                if ( orderItem.getbLocal_img2() )
                {
                    Bitmap bitmap = ImageUtil.loadFixedSizeBitmap(orderItem.getUpload_img2(), 200, 200) ;
                    holder.btn_img_photo2.setImageBitmap(bitmap);
                }
                else
                {
                    String upload_img2 = C.BASE_ROOT_URL + orderItem.getUpload_img2() ;
                    mAQ.id(holder.btn_img_photo2).image(upload_img2, false, false, 68, 0);
                }
            }

            if( orderItem.getUpload_img3().length() > 0 )
            {
                if ( orderItem.getbLocal_img2() )
                {
                    Bitmap bitmap = ImageUtil.loadFixedSizeBitmap(orderItem.getUpload_img3(), 200, 200) ;
                    holder.btn_img_photo3.setImageBitmap(bitmap);
                }
                else
                {
                    String upload_img3 = C.BASE_ROOT_URL + orderItem.getUpload_img3() ;
                    mAQ.id(holder.btn_img_photo3).image(upload_img3, false, false, 68, 0);
                }
            }

            int id = Util.getId(orderItem.getPhoto_bg(), R.drawable.class);
            holder.btn_img_bg.setImageResource(id);

            if( orderItem.getPhoto_cloth().length() > 0 )
            {
                id = Util.getId(orderItem.getPhoto_cloth(), R.drawable.class);
                holder.btn_img_cloth.setImageResource(id);
            }

            if ( orderItem.getOrder_state().equals("2") )
            {
                holder.btn_completed.setVisibility(View.VISIBLE);
                holder.layout_download_result.setVisibility(View.VISIBLE);
                holder.btn_download_result.setOnClickListener(this.onClickListener) ;

                String download_img = orderItem.getDownload_img();
                if( download_img.length() > 0 )
                    download_img = C.BASE_ROOT_URL + download_img ;

                holder.btn_download_result.setTag(download_img) ;
            }
            else if ( orderItem.getOrder_state().equals("3") )
            {
                holder.tv_change_photo1.setVisibility(View.VISIBLE);
                holder.tv_change_photo2.setVisibility(View.VISIBLE);
                holder.tv_change_photo3.setVisibility(View.VISIBLE);

                holder.btn_img_photo1.setTag(position);
                holder.btn_img_photo2.setTag(position);
                holder.btn_img_photo3.setTag(position);

                holder.btn_img_photo1.setOnClickListener(this.onClickListener);
                holder.btn_img_photo2.setOnClickListener(this.onClickListener) ;
                holder.btn_img_photo3.setOnClickListener(this.onClickListener) ;

                if ( orderItem.getbLocal_img1() || orderItem.getbLocal_img2() || orderItem.getbLocal_img3() )
                {
                    holder.btn_upload_img.setVisibility(View.VISIBLE);
                    holder.btn_upload_img.setOnClickListener(this.onClickListener);
                    holder.btn_upload_img.setTag(orderItem.getOrder_id() + '|' + orderItem.getUpload_img1() + '|' + orderItem.getUpload_img2() + '|' + orderItem.getUpload_img3()) ;
                }


            }

            holder.tv_pay_type.setText(orderItem.P_TYPE);
            holder.tv_pay_state.setText(orderItem.pay_state);

            if( orderItem.P_TYPE.equals("가상계좌") )
            {
                holder.lo_bank.setVisibility(View.VISIBLE);
                holder.lo_pay_owner.setVisibility(View.VISIBLE);
                holder.lo_date.setVisibility(View.VISIBLE);
                holder.lo_account.setVisibility(View.VISIBLE);

                holder.tv_pay_account.setText(orderItem.P_VACT_NUM);
                holder.tv_pay_date.setText(orderItem.P_VACT_DATE);
                holder.tv_pay_owner.setText(orderItem.P_VACT_NAME);
                holder.tv_pay_bank.setText(orderItem.P_VACT_BANK_CODE);
            }
            else
            {
                holder.lo_bank.setVisibility(View.GONE);
                holder.lo_pay_owner.setVisibility(View.GONE);
                holder.lo_date.setVisibility(View.GONE);
                holder.lo_account.setVisibility(View.GONE);
            }

            if ( !orderItem.getMsg().equals("") )
            {
                holder.btn_memo.setVisibility(View.VISIBLE);
                holder.layout_msg.setVisibility(View.VISIBLE);
                holder.tv_msg.setText(orderItem.getMsg()) ;
            }

            holder.btn_content_open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToggleContents(holder);
                }
            });

            holder.btn_content_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToggleContents(holder);
                }
            });
        }
        else if ( layout == R.layout.item_address )
        {
            ListViewItem listViewItem = (ListViewItem) data.get(position) ;

            holder.tv_zip.setText(listViewItem.getID());
            holder.tv_addr.setText(listViewItem.getLabel());
        }
        else
        {
            ListViewItem listViewItem = (ListViewItem) data.get(position) ;

            holder.tv_item.setText(listViewItem.getLabel());
        }

        holder.id = position ;
        return convertView;
    }

    public void ToggleContents(ViewHolder viewHolder)
    {
        if (viewHolder != null)
        {
            if (viewHolder.btn_content_open.getVisibility() == View.VISIBLE)
            {
                viewHolder.btn_content_open.setVisibility(View.GONE);
                viewHolder.btn_content_close.setVisibility(View.VISIBLE);
                viewHolder.layout_contents.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btn_content_close.setVisibility(View.GONE);
                viewHolder.btn_content_open.setVisibility(View.VISIBLE);
                viewHolder.layout_contents.setVisibility(View.GONE);
            }
        }
    }

    public void ToggleContents(ListView listView, int position)
    {
        ViewHolder viewHolder = (ViewHolder) getViewByPosition(listView, position).getTag() ;

        ToggleContents(viewHolder) ;
    }

    public View getViewByPosition(ListView listView, int position)
    {
        int firstPosition = listView.getFirstVisiblePosition();
        int lastPosition = listView.getLastVisiblePosition();

        if ((position < firstPosition) || (position > lastPosition))
            return null;

        return listView.getChildAt(position - firstPosition);
    }

    /*public View getViewByPosition(ListView listView, int position)
    {
        try {
            final int firstListItemPosition = listView
                    .getFirstVisiblePosition();
            final int lastListItemPosition = firstListItemPosition
                    + listView.getChildCount() - 1;

            if (position < firstListItemPosition || position > lastListItemPosition)
            {
                return listView.getAdapter().getView(position, null, listView);
            }
            else
            {
                final int childIndex = position - firstListItemPosition;
                return listView.getChildAt(childIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    public void deselect_item(GridView gridView)
    {
        if (old_position > -1)
        {
            ViewGroup v = (ViewGroup) gridView.getChildAt(old_position);

            if (v != null)
            {
                for (int i = 0; i < v.getChildCount(); ++i) {
                    v.getChildAt(i).setSelected(false);
                }
            }
        }
    }

    public void select_item(GridView gridView, int position)
    {
        ViewGroup v = (ViewGroup) gridView.getChildAt(position);

        if (v != null)
        {
            for (int i = 0; i < v.getChildCount(); ++i) {
                v.getChildAt(i).setSelected(true);
            }

            setOldPosition(position);
        }

        notifyDataSetChanged();
    }

    public void toggle_select_item(GridView gridView, int position)
    {
        deselect_item(gridView) ;
        select_item(gridView, position) ;
    }

    public void reselect_item(final GridView gridView, final int position)
    {
        if ( position == -1 )
            return ;

        ViewTreeObserver viewTreeObserver = gridView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    gridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    gridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                if (getOldPosition() > -1)
                    gridView.getChildAt(getOldPosition()).setSelected(false);

                select_item(gridView, position) ;
            }
        });
    }

    class ViewHolder
    {
        int id ;

        TextView tv_item ;

        ImageView img_bg ;
        CheckBox check_bg ;

        TextView tv_title ;
        TextView tv_date ;
        WebView wv_contents ;

        ImageButton btn_content_open ;
        ImageButton btn_content_close ;

        RelativeLayout layout_contents ;
        LinearLayout layout_download_result, layout_msg ;

        TextView tv_zip ;
        TextView tv_addr ;

        TextView tv_total_price ;
        TextView tv_order_name ;
        TextView tv_order_hp ;
        TextView tv_order_addr ;

        TextView tv_order_price ;
        TextView tv_delivery_price ;
        TextView tv_total_price2 ;

        ImageButton btn_img_bg, btn_img_cloth ;
        ImageButton btn_img_photo1, btn_img_photo2, btn_img_photo3 ;
        Button btn_download_result, btn_upload_img ;

        TextView tv_change_photo1, tv_change_photo2, tv_change_photo3 ;
        TextView tv_change_bg, tv_change_cloth ;
        TextView tv_msg ;

        ImageView icon_announcement ;
        ImageView icon_event ;

        TextView tv_pay_type;
        TextView tv_pay_state;

        TextView tv_pay_account, tv_pay_date, tv_pay_owner, tv_pay_bank;

        LinearLayout    lo_account, lo_date, lo_pay_owner, lo_bank;
        //TextView tv_memo, tv_completed ;
        ImageButton btn_memo, btn_completed ;
    }
}
