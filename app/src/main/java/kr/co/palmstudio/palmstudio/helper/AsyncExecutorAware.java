package kr.co.palmstudio.palmstudio.helper;

/**
 * Created by Administrator on 2015-10-12.
 */
public interface AsyncExecutorAware<T>
{
    public void setAsyncExecutor(AsyncExecutor<T> asyncExecutor);
}
