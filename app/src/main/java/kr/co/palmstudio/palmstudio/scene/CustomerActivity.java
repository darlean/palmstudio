package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.NoticeItem;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CItemData;
import kr.co.palmstudio.palmstudio.util.ItemDataAdapter;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-11.
 */
public class CustomerActivity extends Activity implements View.OnClickListener
{
    ListViewAdapter adapter;
    ArrayList<NoticeItem> noticeList ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(R.string.activity_customer_title) ;

        noticeList = new ArrayList<NoticeItem>();
        findViewById(R.id.btn_back).setOnClickListener(this);

        Member.getUsage().getCustomer(this, this, "recvCustomer");
    }

    public void recvCustomer(int _statusCode, JSONObject _response)
    {
        if ( _response == null )
            return ;

        Log.i("Notice", _response.toString());

        try {
            JSONObject jsonObject = _response ;

            Iterator iterator = jsonObject.keys() ;//Util.sortKeys(jsonObject.keys()).iterator();

            noticeList.clear();

            while(iterator.hasNext())
            {
                String key = (String) iterator.next();

                if ( !key.equals("session_result") && !key.equals("is_login") )
                {
                    JSONObject json = jsonObject.getJSONObject(key);
                    noticeList.add(new NoticeItem(Integer.parseInt(key), json.optString("subject"), json.optString("content"), json.optString("datetime")));
                }
            }

            adapter = new ListViewAdapter(this, noticeList, R.layout.item_notice);

            final ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    adapter.ToggleContents(listView, position);
                }
            });

            if ( noticeList.size() > 0 )
            {
                TextView textView = (TextView) findViewById(R.id.list_empty_tv) ;
                textView.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            Util.alertDlg(this, errors.toString());
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;

        }
    }
}
