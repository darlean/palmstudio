package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.data.NoticeItem;
import kr.co.palmstudio.palmstudio.define.Receiver;
import kr.co.palmstudio.palmstudio.helper.IHExceptionHandler;

import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.size_scene.SizeActivity;
import kr.co.palmstudio.palmstudio.usage_scene.UsageActivity;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.Util;

import com.androidquery.AQuery;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends Activity implements View.OnClickListener {

    private AQuery          m_aq;
    
    ArrayList<NoticeItem> noticeList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (savedInstanceState != null)
            Thread.setDefaultUncaughtExceptionHandler(new IHExceptionHandler(this, this, IntroActivity.class));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_aq = new AQuery(this);

        TextView login_tv = (TextView) findViewById(R.id.login_tv) ;


        if ( Member.getInstance().isLogin() )
        {
            login_tv.setText(R.string.activity_my_info_title) ;
        }
        else
        {
            login_tv.setText(R.string.join_login) ;
        }

        noticeList = new ArrayList<NoticeItem>();

        findViewById(R.id.main_top_left_ib).setOnClickListener(this);
        findViewById(R.id.main_top_alarm_ib).setOnClickListener(this);

        findViewById(R.id.main_top_alarm_ib).setVisibility(View.GONE);
        findViewById(R.id.top_menu_count_rl).setVisibility(View.GONE);

        findViewById(R.id.login_rl).setOnClickListener(this);
        findViewById(R.id.order_rl).setOnClickListener(this);
        findViewById(R.id.notice_rl).setOnClickListener(this);
        findViewById(R.id.event_rl).setOnClickListener(this);
        findViewById(R.id.customer_rl).setOnClickListener(this);
        findViewById(R.id.guide_rl).setOnClickListener(this);
        findViewById(R.id.setting_rl).setOnClickListener(this);

     /*   findViewById(R.id.btn_gallery).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_order_list).setOnClickListener(this);*/

        findViewById(R.id.btn_usage).setOnClickListener(this);
        findViewById(R.id.btn_size).setOnClickListener(this);

        findViewById(R.id.btn_emergency).setOnClickListener(this);
        findViewById(R.id.btn_time).setOnClickListener(this);
        findViewById(R.id.btn_guide).setOnClickListener(this);
        findViewById(R.id.btn_price).setOnClickListener(this);
        findViewById(R.id.btn_delivery).setOnClickListener(this);
        findViewById(R.id.btn_group).setOnClickListener(this);

        // check a token
        //Member.getSession().get( this, "app/android/login_test" );
        //Member.getUsage().getNotice(this, this, "recvNotice");
        //Member.getUsage().getMainInformation(this, this, "recvInformation");
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Re-initialize if camera already released
        //Member.getUsage().getMainInformation(this, this, "recvInformation");
        Member.getUsage().getNotificationCount(this, this, "getNotificationCount");
        Member.getUsage().getNotice(this, this, "recvNotice");

        TextView login_tv = (TextView) findViewById(R.id.login_tv) ;
        RelativeLayout order_rl = (RelativeLayout) findViewById(R.id.order_rl) ;

        if ( Member.getInstance().isLogin() )
        {
            login_tv.setText(R.string.activity_my_info_title) ;
        }
        else
        {
            login_tv.setText(R.string.join_login) ;
        }
    }

    public void getNotificationCount(int _statusCode, JSONObject _response)
    {
        if ( _response == null )
            return ;

        Log.i("getNotificationCount", _response.toString());

        try {
            Integer Count = _response.getInt("count");
            if( Count != 0 ){
                findViewById(R.id.main_top_alarm_ib).setVisibility(View.VISIBLE);
                findViewById(R.id.top_menu_count_rl).setVisibility(View.VISIBLE);
                m_aq.id(R.id.top_menu_count_tv).text(Integer.toString(Count));
            }
            else
            {
                findViewById(R.id.main_top_alarm_ib).setVisibility(View.GONE);
                findViewById(R.id.top_menu_count_rl).setVisibility(View.GONE);
            }

        } catch (Exception e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            Util.alertDlg(this, errors.toString());
        }
    }

    public void recvNotice(int _statusCode, JSONObject _response)
    {
        if ( _response == null )
            return ;

        Log.i("Notice", _response.toString());

        try {
            JSONObject jsonObject = _response ;

            Iterator iterator = jsonObject.keys() ;//Util.sortKeys(jsonObject.keys()).iterator();

            while(iterator.hasNext())
            {
                String key = (String) iterator.next();

                if ( !key.equals("session_result") && !key.equals("is_login") )
                {
                    JSONObject json = jsonObject.getJSONObject(key);
                    noticeList.add(new NoticeItem(Integer.parseInt(key), json.optString("subject"), json.optString("content"), json.optString("datetime")));
                }
            }

        } catch (Exception e)
        {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            Util.alertDlg(this, errors.toString());
        }
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            AlertDialog.Builder ab = new AlertDialog.Builder( this );
            ab.setMessage( "손바닥 스튜디오를 종료하시겠습니까?" );
            ab.setNegativeButton(android.R.string.cancel, null);
            ab.setPositiveButton(android.R.string.ok,  new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            ab.setTitle( "종료" );
            ab.show();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.main_top_left_ib:
                Log.i("TEST", "R.id.main_top_left_ib") ;
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
                break;

            case R.id.main_top_alarm_ib:
                //Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                Intent intent = new Intent(getApplicationContext(), NotifyActivity.class);
                intent.putExtra("TYPE", "") ;
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_camera:
                intent = new Intent(getApplicationContext(), CameraActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_gallery:
                Intent mIntent = new Intent(this, CustomGallery.class);
                mIntent.putExtra(
                        Receiver.EXTRAS_ACTION, Receiver.ACTION_CHOSE_SINGLE_FILE);
//                mIntent.putExtra(
//                        Receiver.EXTRAS_CASE_RECEIVER, Receiver.case_camera_in_setting);

                startActivity(mIntent);

                break ;

            case R.id.btn_order_list:
                intent = new Intent(getApplicationContext(), OrderListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_usage:
                intent = new Intent(getApplicationContext(), UsageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_size:
                intent = new Intent(getApplicationContext(), SizeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_emergency:
            case R.id.btn_time:
            case R.id.btn_guide:
            case R.id.btn_price:
            case R.id.btn_delivery:
            case R.id.btn_group:
                int noticeIdx = 0 ;

                if ( view.getId() == R.id.btn_emergency )
                {
                    noticeIdx = 0 ;
                }
                else if ( view.getId() == R.id.btn_time )
                {
                    noticeIdx = 1 ;
                }
                else if ( view.getId() == R.id.btn_guide )
                {
                    noticeIdx = 2 ;
                }
                else if ( view.getId() == R.id.btn_price )
                {
                    noticeIdx = 3 ;
                }
                else if ( view.getId() == R.id.btn_delivery )
                {
                    noticeIdx = 4 ;
                }
                else if ( view.getId() == R.id.btn_group )
                {
                    noticeIdx = 5 ;
                }

                NoticeItem noticeItem = noticeList.get(noticeIdx);

                intent = new Intent(getApplicationContext(), NoticePopUpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("URL", noticeItem.getNOTICE_CONTENTS()) ;
                startActivity(intent);

                break ;

            case R.id.login_rl:
            case R.id.order_rl:
            case R.id.notice_rl:
            case R.id.event_rl:
            case R.id.customer_rl:
            case R.id.guide_rl:
            case R.id.setting_rl:
                onSlideMenuSelected(view) ;
                break ;
        }
    }

    public void onSlideMenuSelected(View view)
    {
        int id = view.getId() ;

        if ( id == R.id.login_rl )
        {
            Intent intent;
            if ( Member.getInstance().isLogin() )
            {
                intent = new Intent(getApplicationContext(), MyInfoActivity.class);
            }
            else
            {
                intent = new Intent(getApplicationContext(), LoginActivity.class);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);


        }
        else if ( id == R.id.order_rl )
        {
            Intent intent = new Intent(getApplicationContext(), OrderListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else if ( id == R.id.notice_rl )
        {
            Intent intent = new Intent(getApplicationContext(), NoticeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else if ( id == R.id.event_rl )
        {
            Intent intent = new Intent(getApplicationContext(), EventActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else if ( id == R.id.customer_rl )
        {
            Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else if ( id == R.id.guide_rl )
        {
            Intent intent = new Intent(getApplicationContext(), GuideActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else if ( id == R.id.setting_rl )
        {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

}
