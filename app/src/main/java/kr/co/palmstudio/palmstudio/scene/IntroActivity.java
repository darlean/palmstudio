package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.cookie.Cookie;
import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpRequest;
import kr.co.palmstudio.palmstudio.helper.HttpRequestListener;
import kr.co.palmstudio.palmstudio.member.Member;

import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.util.List;
/*
import hbn.co.kr.hotple.data.UserInformation;
import hbn.co.kr.hotple.network.IHNetwork;
import hbn.co.kr.hotple.utils.Utils;
import hbn.co.kr.hotple.R;
import hbn.co.kr.hotple.alert.IHAlertDialog;
import hbn.co.kr.hotple.data.CategoryManager;
import hbn.co.kr.hotple.data.VersionCheckData;
import hbn.co.kr.hotple.gcm.GcmRegister;
import hbn.co.kr.hotple.helper.MyLocationHelper;
import hbn.co.kr.hotple.network.IHHttpResponseHandler;
import hbn.co.kr.hotple.utils.JKLocationManager;*/

/**
 * Created by jinkookseok on 14. 11. 27..
 */
public class IntroActivity extends Activity {
    final Handler mHandler = new Handler();
    private final long IntroDefaultTime = 1000;
    ImageView       character_iv;
    ImageView       long_iv;
    ImageView       short_iv;
    FrameLayout     city_fl;
    private long mStartTime;

    Intent          mIntent;

    boolean         mNetworkComplete = false;
    boolean         mLocationComplete = false;

    boolean         mPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mStartTime = System.currentTimeMillis();
        mIntent = new Intent();

        // 멤버 초기화
        Member.getInstance().init(this);

        startWithNetworkCheck();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mPaused = true;
        finish();
    }

    @Override
    public void onBackPressed() {
    }

    public void startWithNetworkCheck()
    {
        if (!Member.getSession().isOnline(this))
        {
            showNetworkState("네트워크 문제로 연결이 지연되고 있습니다. 네트워크 확인 후 다시 시도해 주세요.");
        }
        else
        {
            // 토큰 전송 시작
            //sendToken();
            //mNetworkComplete = true;
            //finishIntro(RESULT_OK);
            startVersionCheck();
        }
    }

    public void startVersionCheck() {

        String currentVersion = "";
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Member.getUsage().check_version(this, currentVersion, this, "recvVersion");
        /*IHNetwork.unBlockRequest().versions(currentVersion, getPackageName(), new IHHttpResponseHandler() {
            @Override
            public void onResult(int statusCode, Throwable throwable, boolean fail) {
                if (fail) {
                    showNetworkState(throwable.getMessage());
                } else {
                    final VersionCheckData checkData = VersionCheckData.parseFromNetwork(getResult());
                    if (checkData.needShowAlert()) {
                        checkData.showAlert(IntroActivity.this);
                    } else {
                        requestCategory();
                    }
                }
            }
        });*/
    }

    // 토큰 전송
    public void sendToken()
    {
        // check a token
        Member.getUsage().check_token(getApplicationContext(), this, "recvToken");
    }

    // received a token
    public void recvToken(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            String currentVersion = "";
            try {
                currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Member.getUsage().check_version( this, currentVersion, this, "recvVersion");
        }
        else
        {
            showNetworkState("네트워크 문제로 연결이 지연되고 있습니다. 네트워크 확인 후 다시 시도해 주세요.");
        }
    }

    public void recvVersion(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            Log.i("check_version", _response.toString());

            try
            {
                boolean reseult = _response.getBoolean("result");
                String reason = _response.getString("reason");
                String curr_version = _response.getString("version");

                if( reseult )
                {
                    //mNetworkComplete = true;
                    finishIntro(RESULT_OK);
                }
                else
                {
                    AlertDialog.Builder ab = new AlertDialog.Builder(this);
                    ab.setMessage("업데이트된 버전("+curr_version+")이 있습니다.\n업데이트 후 이용해 주세요.");
                    ab.setNegativeButton("업데이트", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Intent intent = null;
                            try {
                                intent = Intent.parseUri(C.DOWNLOAD_URI, 0);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                            IntroActivity.this.startActivity(intent);
                        }
                    });

                    ab.setPositiveButton("종료", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    ab.setTitle("업데이트 알림");
                    ab.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Toast.makeText( this, _response.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void showNetworkState(String message)
    {
        /*Utils.noticeDialog(this, message, "다시시도", "종료",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startWithNetworkCheck();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }
        );*/

        finish();
    }

    private void finishIntro(int resultCode)
    {
        if (mPaused) return;

        //if (!mNetworkComplete) return;

        setResult(resultCode);
        long remainTime = IntroDefaultTime - (System.currentTimeMillis() - mStartTime);

        if (remainTime > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startMainActivity();
                }
            }, remainTime);
        } else {
            startMainActivity();
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(mIntent);
        startActivity(intent);
        finish();
    }
}
