package kr.co.palmstudio.palmstudio.size_scene;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.scene.MainActivity;
import kr.co.palmstudio.palmstudio.scene.OrderActivity;
import kr.co.palmstudio.palmstudio.scene.TipActivity;
import kr.co.palmstudio.palmstudio.util.CommonDialog;

/**
 * Created by darlean on 2016. 5. 6..
 */
public class SizeActivity extends AppCompatActivity implements View.OnClickListener, Size_2_5x3Fragment.OnClickListener, Size_3_5x4_5Fragment.OnClickListener, Size_3x4Fragment.OnClickListener, Size_5x5Fragment.OnClickListener, Size_5x7Fragment.OnClickListener
{
    private CommonDialog commonDialog ;
    private int selType ;
    private String strType ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_size_main);

        commonDialog = new CommonDialog(this, R.layout.dialog_layout_2) ;
        commonDialog.setCancelable(false);

        commonDialog.findViewById(R.id.btn_ok).setOnClickListener(this);
        commonDialog.findViewById(R.id.btn_cancel).setOnClickListener(this);

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_guide).setOnClickListener(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("2.5x3"));
        tabLayout.addTab(tabLayout.newTab().setText("3x4"));
        tabLayout.addTab(tabLayout.newTab().setText("3.5x4.5"));
        tabLayout.addTab(tabLayout.newTab().setText("5x5"));
        tabLayout.addTab(tabLayout.newTab().setText("5x7"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final TabsPagerAdapter adapter = new TabsPagerAdapter
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View view)
    {
        if ( view.getId() == R.id.btn_back )
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if ( view.getId() == R.id.btn_guide )
        {
            Intent intent = new Intent(getApplicationContext(), TipActivity.class);
            startActivity(intent);
            finish();
        }
        else if ( view.getId() == R.id.btn_ok )
        {
            commonDialog.dismiss();

            Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
            intent.putExtra("TYPE", "photoType") ;
            intent.putExtra("SelType", this.selType) ;
            intent.putExtra("StrType", this.strType) ;
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);

            intent = new Intent(getApplicationContext(), TipActivity.class);
            startActivity(intent);
        }
        else if ( view.getId() == R.id.btn_cancel )
        {
            // 정보 초기화
            this.selType = -1 ;
            this.strType = "" ;

            commonDialog.dismiss();
        }
    }

    public void onSelectTypeClick(View v, int selType, String strType)
    {
        // 정보 저장
        this.selType = selType ;
        this.strType = strType ;

        commonDialog.setMessage(strType + "로 주문 하시겠습니까?");
        commonDialog.show();
    }
}
