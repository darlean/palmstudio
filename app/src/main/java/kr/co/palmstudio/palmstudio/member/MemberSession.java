package kr.co.palmstudio.palmstudio.member;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.Reference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpClientHelper;
import kr.co.palmstudio.palmstudio.helper.HttpRequest;
import kr.co.palmstudio.palmstudio.helper.HttpRequestListener;
import kr.co.palmstudio.palmstudio.scene.FindPWActivity;
import kr.co.palmstudio.palmstudio.scene.LoginActivity;
import kr.co.palmstudio.palmstudio.scene.MainActivity;

/**
 * Created by 홍군 on 2016-02-12.
 */
public class MemberSession
{
    public MemberSession()
    {

    }

    public String findValueFromCookies( List<Cookie> _cookies, String _name )
    {
        for (Cookie cookie : _cookies )
        {
            if (cookie.getName().equals(_name))
            {
                String value = cookie.getValue();

                return  ( value != null && value.length() > 0 ) ? value : null;
            }
        }

        return null;
    }

    public HttpRequest createRequest( Context ctx, Object handler, String callback )
    {
        HttpRequest request = new HttpRequest( new HttpRequestListener( ctx, handler, callback )
        {
            @Override
            public void response(int _statusCode, JSONObject _response) throws JSONException
            {
                if( _response != null )
                {
                    //Log.i("MemberSession", _response.toString());
                    if( !_response.getBoolean("is_login") && Member.getInstance().isLogin() )
                    {
                        Member.getInstance().clear();
                    }

                    this.invokeMethod(_statusCode, _response);
                }
                else
                {
                    Log.e("MemberSession", "ErrorCode : " + _statusCode );
                }
            }

            @Override
            public void response_error(int _statusCode, String _response) throws JSONException
            {
                Context ctx = getContext();
                if( ctx == null ) return;

                AlertDialog.Builder ab = null;
                ab = new AlertDialog.Builder( ctx );
                ab.setMessage( Html.fromHtml(_response) );
                ab.setPositiveButton(android.R.string.ok, null);
                ab.setTitle( "통신 에러" );
                ab.show();
            }

            @Override
            public void session_expired(JSONObject _response) throws JSONException
            {
                Context ctx = this.getCallbackHandler().getContext();
                if( ctx != null )
                {
                    Intent intent = new Intent(ctx, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    ctx.startActivity(intent);
                }

                Log.i("MemberSession", _response.toString());
            }

            @Override
            public void session_remaked(JSONObject _response) throws JSONException
            {
                Context ctx = this.getCallbackHandler().getContext();
                if( ctx != null )
                {

                }

                Log.i("MemberSession", _response.toString());
            }
        } );

        return request;
    }

    public void check_auto_login( Context _ctx )
    {
        SharedPreferences setting = _ctx.getSharedPreferences(C.Preference.PREFERENCES_KEY, 0);
        String AUL_KEY = setting.getString(C.Preference.AUL_KEY, "");
        String MEM_ID = setting.getString(C.Preference.MEMBER_USERID, "");
        if( AUL_KEY.length() > 0  && MEM_ID.length() > 0 )
        {
            //Toast.makeText(_ctx, AUL_KEY + " " + MEM_ID, Toast.LENGTH_LONG).show();

            AsyncHttpClient client = HttpClientHelper.getInstance();
            PersistentCookieStore myCookieStore = new PersistentCookieStore(_ctx);

            myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
            myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_USERID, "") );

            myCookieStore.addCookie(new BasicClientCookie(C.Preference.AUL_KEY, AUL_KEY));
            myCookieStore.addCookie(new BasicClientCookie(C.Preference.MEMBER_USERID, MEM_ID));

            client.setCookieStore(myCookieStore);
            //getUsage().debugCookie(myCookieStore);
        }


    }

    public void post_with_dialog( Context _ctx, String _message, String _relativeUrl, RequestParams _param, Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.post_with_dialog(_ctx, _message, _relativeUrl, _param);
    }

    public void post_with_dialog( Context _ctx, String _message, String _relativeUrl, Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.post_with_dialog(_ctx, _message, _relativeUrl );
    }

    public void post( Context _ctx, String _relativeUrl, RequestParams _param, Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.post(_relativeUrl, _param);
    }

    public void post( Context _ctx, String _relativeUrl,  Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.post(_relativeUrl);
    }

    public void get_with_dialog( Context _ctx, String _message, String _relativeUrl, RequestParams _param, Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.get_with_dialog(_ctx, _relativeUrl, _message, _param);
    }

    public void get( Context _ctx, String _relativeUrl, RequestParams _param, Object handler, String callback )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.get(_relativeUrl, _param);
    }

    public void get( Context _ctx, String _relativeUrl, Object handler, String callback )
    {
        check_auto_login(_ctx);

        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, handler, callback);

        request.get(_relativeUrl);
    }

    public void get( Context _ctx, String _relativeUrl )
    {
        check_auto_login(_ctx);

        HttpRequest request = createRequest(_ctx, null, null);

        request.get(_relativeUrl);
    }

    // 온라인 가능한지 체크
    public boolean isOnline(Context ctx)
    {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
