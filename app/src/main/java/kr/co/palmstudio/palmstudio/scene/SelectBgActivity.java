package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.data.BgItem;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.ExpandableHeightGridView;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-11.
 */
public class SelectBgActivity extends Activity implements View.OnClickListener
{
    CommonToast toast ;

    ListViewAdapter adapter;
    ArrayList<BgItem> bgList ;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bg);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText("배경 선택") ;

        toast = new CommonToast(getApplicationContext()) ;

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);

        processIntent();
    }

    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
        processIntent();
    }

    private void processIntent()
    {
        Intent intent = getIntent();
        if( intent.getExtras().getString("TYPE").equals("BG") )
        {
            bgList = new ArrayList<BgItem>();
            for ( int i = 1 ; i <= 15 ; i++ )
            {
                String Name = "photo_bg_" + i ;
                int id = Util.getId( Name, R.drawable.class);
                bgList.add(new BgItem(id, Name));
            }
            adapter = new ListViewAdapter(this, bgList, R.layout.item_bg);

            final ExpandableHeightGridView expandableHeightGridView = (ExpandableHeightGridView) findViewById(R.id.gridView);
            expandableHeightGridView.setAdapter(adapter);
            expandableHeightGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.toggle_select_item(expandableHeightGridView, position);
                }
            });

            expandableHeightGridView.setExpanded(true);

            adapter.reselect_item(expandableHeightGridView, 0) ;
        }
        else if( intent.getExtras().getString("TYPE").equals("CLOTH") )
        {
            boolean bMale = intent.getBooleanExtra("SEX", true) ;

            int i = 1 ;
            int max = 11 ;

            if ( !bMale )
            {
                i = 12 ;
                max = 18 ;
            }

            bgList = new ArrayList<BgItem>();
            for ( ; i <= max ; i++ )
            {
                String Name = "cloth_" + i ;
                int id = Util.getId( Name, R.drawable.class);
                bgList.add(new BgItem(id, Name));
            }

            adapter = new ListViewAdapter(this, bgList, R.layout.item_bg);
            final ExpandableHeightGridView expandableHeightGridView = (ExpandableHeightGridView) findViewById(R.id.gridView);
            expandableHeightGridView.setAdapter(adapter);
            expandableHeightGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.toggle_select_item(expandableHeightGridView, position);
                }
            });

            expandableHeightGridView.setExpanded(true);

            adapter.reselect_item(expandableHeightGridView, 0) ;
        }
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId() ;

        if ( id == R.id.btn_back )
        {
            Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
            intent.putExtra("TYPE", "") ;
            startActivity(intent);
            finish();
        }
        else if ( id == R.id.btn_select )
        {
            Intent intent = new Intent() ;
            BgItem item = (BgItem)adapter.getItem(adapter.getOldPosition());
            if( item != null )
                intent.putExtra("BG", item.getBgName() ) ;
            else
                intent.putExtra("BG", "Non Selected");

            setResult(RESULT_OK, intent) ;
            finish();
        }
    }
}
