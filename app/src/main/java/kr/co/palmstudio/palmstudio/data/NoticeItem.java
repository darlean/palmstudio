package kr.co.palmstudio.palmstudio.data;

/**
 * Created by darlean on 2016. 3. 3..
 */
public class NoticeItem
{
    private int NOTICE_IDX ;

    private String NOTICE_TITLE ;
    private String NOTICE_CONTENTS ;
    private String NOTICE_DATE ;
    private String NOTICE_TYPE ; // 이벤트인지 아닌지

    public NoticeItem ( int NOTICE_IDX, String NOTICE_TITLE, String NOTICE_CONTENTS, String NOTICE_DATE )
    {
        this.NOTICE_IDX = NOTICE_IDX ;
        this.NOTICE_TITLE = NOTICE_TITLE ;
        this.NOTICE_CONTENTS = NOTICE_CONTENTS;
        this.NOTICE_DATE = NOTICE_DATE ;
    }

    public NoticeItem ( int NOTICE_IDX, String NOTICE_TITLE, String NOTICE_CONTENTS, String NOTICE_DATE, String NOTICE_TYPE )
    {
        this.NOTICE_IDX = NOTICE_IDX ;
        this.NOTICE_TITLE = NOTICE_TITLE ;
        this.NOTICE_CONTENTS = NOTICE_CONTENTS;
        this.NOTICE_DATE = NOTICE_DATE ;
        this.NOTICE_TYPE = NOTICE_TYPE ;
    }

    public int getNOTICE_IDX()
    {
        return this.NOTICE_IDX ;
    }

    public String getNOTICE_TITLE()
    {
        return this.NOTICE_TITLE ;
    }

    public String getNOTICE_CONTENTS()
    {
        return this.NOTICE_CONTENTS ;
    }

    public String getNOTICE_DATE()
    {
        return this.NOTICE_DATE ;
    }

    public String getNOTICE_TYPE() { return this.NOTICE_TYPE ; }
}
