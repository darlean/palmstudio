package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.helper.HttpClientHelper;
import kr.co.palmstudio.palmstudio.helper.HttpRequest;
import kr.co.palmstudio.palmstudio.helper.HttpRequestListener;
import kr.co.palmstudio.palmstudio.member.Member;

/**
 * Created by Administrator on 2016-01-19.
 */
public class LoginActivity extends Activity implements View.OnClickListener
{
    private EditText        m_editID;
    private EditText        m_editPassword;
    private CheckBox        m_chkAutoLogin;
    private AQuery          m_aq;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        m_aq = new AQuery(this);

        // set text
        m_aq.id(R.id.tv_title).text(R.string.activity_login_title);
        //m_aq.id(R.id.edit_id).text("test");
        //m_aq.id(R.id.edit_pw).text("f118");

        // register listener
        m_aq.id(R.id.btn_login).clicked(this, "requestLogin");
        m_aq.id(R.id.btn_back).clicked(this);
        m_aq.id(R.id.btn_find_pw).clicked(this);
        m_aq.id(R.id.btn_join).clicked(this);

        m_editID = m_aq.find(R.id.edit_id).getEditText();
        m_editPassword = m_aq.find(R.id.edit_pw).getEditText();
        m_chkAutoLogin = m_aq.find(R.id.cb_auto_login ).getCheckBox();
    }

    public void requestLogin(View view)
    {
        String id = m_editID.getText().toString();
        String password = m_editPassword.getText().toString();
        boolean autoLogin = m_chkAutoLogin.isChecked();

        if (id == null || id.length() == 0) {
            Toast.makeText( this, "아이디를 입력해주세요", Toast.LENGTH_SHORT ).show();
            return;
        }

        if (password == null || password.length() < 4) {
            Toast.makeText( this, "패스워드를 입력해주세요", Toast.LENGTH_SHORT ).show();
            return;
        }

        Member.getUsage().login(this, id, password, autoLogin, this, "recvLogin");
    }

    // received to login
    public void recvLogin(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            try
            {
                if( Member.getUsage().onLoginResult( getApplicationContext(), _response ) )
                {
                    //Intent intent = new Intent(this, MainActivity.class);
                    //startActivity(intent);
                    finish();
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            Log.i("Login", _response.toString() );
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;

            case R.id.btn_find_pw:
                intent = new Intent(getApplicationContext(), FindPWActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break ;

            case R.id.btn_join:
                intent = new Intent(getApplicationContext(), JoinActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break ;
        }
    }


}