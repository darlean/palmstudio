package kr.co.palmstudio.palmstudio.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by darlean on 2016. 1. 29..
 */
public class SexDialog extends Dialog implements View.OnClickListener
{
    public SexDialog(Context context)
    {
        super(context, R.style.FullHeightDialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RelativeLayout root = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.dialog_layout_sex, null);
        setContentView(root);

        findViewById(R.id.btn_male).setOnClickListener(this);
        findViewById(R.id.btn_female).setOnClickListener(this);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.none:
            case R.id.btn_male:
            case R.id.btn_female:
                dismiss();
                break ;
        }
    }
}
