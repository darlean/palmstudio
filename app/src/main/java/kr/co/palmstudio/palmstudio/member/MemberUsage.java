package kr.co.palmstudio.palmstudio.member;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.protocol.ClientContext;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import cz.msebera.android.httpclient.protocol.HttpContext;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpClientHelper;

/**
 * Created by 홍군 on 2016-02-14.
 */
public class MemberUsage
{
    private Member      m_parent = null;

    private MemberUsage()   {}
    public MemberUsage( Member _parent )
    {
        m_parent = _parent;
    }

    public Member getParent()           { return m_parent; }
    public MemberSession getSession()   { return m_parent.getSession(); }

    // --------------------------------------------------------------------------------------------
    // usage methods
    // --------------------------------------------------------------------------------------------

    public void debugCookie( PersistentCookieStore myCookieStore )
    {
        String cookieString = "";
        for (Cookie cookie : myCookieStore.getCookies())
        {
            cookieString += cookie.getName() + "=" + cookie.getValue() + ";";
        }

        Log.i("check_token_cookie", cookieString);
    }

    // check a token from server
    public void check_token( Context _ctx, Object _handler, String _callback )
    {
        getSession().get(_ctx, "app/check_device/check_token", _handler, _callback);
    }

    // check the app version
    public void check_version( Context _ctx, String _version, Object _handler, String _callback )
    {
        // 버전 체크
        RequestParams params = new RequestParams();
        params.put("version", _version );

        getSession().post(_ctx, "main/check_version", params, _handler, _callback);
    }

    // login
    public void login( Context _ctx, final String _id, final String _pwd, final boolean _autologin, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("mb_id", _id);
        params.put("mb_password", _pwd);
        if( _autologin )
            params.put("autologin", true );

        Log.i("Member", "login : " + params.toString());

        getSession().post_with_dialog(_ctx, "로그인 중입니다.", "login", params, _handler, _callback);

        // 로그인 정보 초기화
        getParent().clear();
    }

    // login complete
    public boolean onLoginResult( Context _ctx, JSONObject _result ) throws JSONException
    {
        /*AsyncHttpClient client = HttpClientHelper.getInstance();
        PersistentCookieStore myCookieStore = new PersistentCookieStore(_ctx);
        client.setCookieStore(myCookieStore);*/

        boolean isLogin = _result.getBoolean("result");
        String reason = _result.getString("reason");

        if( isLogin )
        {
            JSONObject memInfo = _result.getJSONObject("member");

            boolean autoLogin = memInfo.getBoolean("auto_login");
            String aulKey = memInfo.getString("aul_key");
            String mbID = memInfo.getString("mb_id");
            String mbName = memInfo.getString("mb_name");
            String mbHP = memInfo.getString("mb_hp");
            String mbZip = memInfo.getString("mb_zip");
            String mbAddr1 = memInfo.getString("mb_addr1");
            String mbAddr2 = memInfo.getString("mb_addr2");

            // 자동로그인 필요한 키 값을 저장
            if( autoLogin )
            {
                SharedPreferences setting = _ctx.getSharedPreferences(C.Preference.PREFERENCES_KEY, 0);
                SharedPreferences.Editor editor = setting.edit();
                editor.putString(C.Preference.AUL_KEY, aulKey);
                editor.putString(C.Preference.MEMBER_USERID, mbID );

                editor.putString(C.Preference.MEMBER_NAME, mbName );
                editor.putString(C.Preference.MEMBER_HP, mbHP );
                editor.putString(C.Preference.MEMBER_ZIP, mbZip );
                editor.putString(C.Preference.MEMBER_ADDR1, mbAddr1 );
                editor.putString(C.Preference.MEMBER_ADDR2, mbAddr2 );

               // Toast.makeText(_ctx, aulKey + " " + mbID, Toast.LENGTH_LONG).show();
                editor.commit();

                //myCookieStore.addCookie(new BasicClientCookie(C.Preference.AUL_KEY, aulKey) );
            }
            else
            {
                // 쿠키 초기화
                //myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
            }



            //myCookieStore.addCookie(new BasicClientCookie(C.Preference.MEMBER_USERID, mbID));


            getParent().setAulKey(aulKey);
            getParent().setMemUserID(mbID);
            getParent().setName(mbName);
            getParent().setHP(mbHP);
            getParent().setZip(mbZip);
            getParent().setAddr1(mbAddr1);
            getParent().setAddr2(mbAddr2);

            Log.i("Member", ", UserID : " + mbID);

            //debugCookie(myCookieStore);

            return true;
        }
        else
        {
            // 쿠키 초기화
            //myCookieStore.clear();
            // 쿠키 초기화
            //myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
            //myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_USERID, "") );


            // 로그인 정보 초기화
            getParent().clear();

            // 메세지 표시
            Toast.makeText(_ctx, reason, Toast.LENGTH_LONG).show();

            return false;
        }
    }

    public void logout( Context _ctx, Object _handler, String _callback )
    {
        SharedPreferences setting = _ctx.getSharedPreferences(C.Preference.PREFERENCES_KEY, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.clear();
        editor.commit();

        //getSession().get(_ctx, "login/out", _handler, _callback );
        getSession().post_with_dialog(_ctx, "로그아웃 중입니다.", "login/out", _handler, _callback);
    }

    public void onLogoutResult( Context _ctx )
    {
        /*AsyncHttpClient client = HttpClientHelper.getInstance();
        PersistentCookieStore myCookieStore = new PersistentCookieStore(_ctx);
        client.setCookieStore(myCookieStore);

        // 쿠키 초기화
        myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
        myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_USERID, "") );
        myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_EMAIL, "") );
        myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_PHONE, "") );*/

        // 로그인 정보 초기화
        getParent().clear();

        //debugCookie(myCookieStore);
    }

    public void checkEmail( Context _ctx, String _email, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("email", _email);

        getSession().post_with_dialog(_ctx, "이메일 중복 체크 중입니다.", "join/ajax_email_check", params, _handler, _callback);
    }

    public void certPhone( Context _ctx, String _phone_number, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("phone_number", _phone_number);

        getSession().post_with_dialog(_ctx, "인증번호 요청중 입니다.", "join/ajax_cert_phone", params, _handler, _callback);
    }

    public void confirmCertPhone( Context _ctx, String _serial, String _indert_id, String _phone_number, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("serial", _serial);
        params.put("insert_id", _indert_id);
        params.put("phone_number", _phone_number);

        getSession().post_with_dialog(_ctx, "인증 요청중 입니다.", "join/ajax_confirm_cert_phone", params, _handler, _callback);
    }

    public void findPassword( Context _ctx, String _email, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("email", _email);

        getSession().post_with_dialog(_ctx, "인증번호 요청중 입니다.", "join/forget_pw", params, _handler, _callback);
    }

    public void join( Context _ctx, RequestParams _params, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "회원가입 중 입니다.", "join/update", _params, _handler, _callback);
    }

    public void getMyInfo( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "회원정보를 수신중 입니다", "my_info", _handler, _callback);
    }

    public void getNotice( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "정보를 수신중 입니다.", "board/latest/notice", _handler, _callback);
    }

    public void getEvent( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "정보를 수신중 입니다.", "board/latest/event", _handler, _callback);
    }

    public void getGuide( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "정보를 수신중 입니다.", "board/latest/guide", _handler, _callback);
    }

    public void getCustomer( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "정보를 수신중 입니다.", "board/latest/setting", _handler, _callback);
    }

    public void changePassword( Context _ctx,  String _old_pass, String _new_pass, String _new_pass_re, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("mb_id", Member.getInstance().getMemUserID() );
        params.put("old_password", _old_pass);
        params.put("new_password", _new_pass);
        params.put("new_password_re", _new_pass_re);

        getSession().post_with_dialog(_ctx, "비밀번호를 변경중 입니다.", "my_info/password", params, _handler, _callback);
    }

    public void getNotification( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "알림 정보를 수신중 입니다.", "notification/lists", _handler, _callback);
    }

    public void getNotificationCount( Context _ctx, Object _handler, String _callback )
    {
        getSession().post(_ctx,  "notification/count", _handler, _callback);
    }

    public void getMainInformation( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "새로운 정보를 수신중 입니다.", "main", _handler, _callback);
    }

    public void findAddress( Context _ctx, String _addr, Object _handler, String _callback )
    {
        RequestParams params = new RequestParams();
        params.put("addr", _addr );

        getSession().post_with_dialog(_ctx, "주소를 찾는중 입니다.", "address/find_address", params, _handler, _callback);
    }

    public void leave( Context _ctx, Object _handler, String _callback )
    {
        getSession().post_with_dialog(_ctx, "탈퇴중 입니다", "join/leave", _handler, _callback);
    }

}
