package kr.co.palmstudio.palmstudio.network;

import android.app.ProgressDialog;
import android.content.Context;

import com.androidquery.AQuery;

import org.json.JSONObject;

import java.util.Map;

import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpRequestListener;

/**
 * Created by 홍군 on 2016-02-11.
 */
public class AQueryHelper
{
    private final static int    SESSION_EXPIRED = 0;        // 세션 만료
    private final static int    REQUEST_SUCCESS = 1;        // 요청 성공
    private final static int    SESSION_REMAKED = 2;        // 세션 재생성

    // 크로스 사이트 토큰
    private final static String CSRF_TOKEN_NAME = "csrf_test_name";
    private final static String CSRF_TOKEN = "6e6405edf59d00fea9d9feff915fe34e";

    AQuery                      m_aquery;


    private AQueryHelper()  {}
    public AQueryHelper( AQuery _aq )
    {
        m_aquery = _aq;
    }

    public String getAbsoluteUrl(String relativeUrl)
    {
        return C.BASE_URL + relativeUrl;
    }

    public void ajax_json( String _relative_url, Map<String, Object> _params, Object _handler, String _callback )
    {
        if( m_aquery == null ) return;

        _params.put(CSRF_TOKEN_NAME, CSRF_TOKEN);

        m_aquery.ajax( getAbsoluteUrl(_relative_url) , _params, JSONObject.class, _handler, _callback );
    }
}
