package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.data.OrderItem;
import kr.co.palmstudio.palmstudio.member.Member;

import kr.co.palmstudio.palmstudio.util.CommonDialog;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.ListViewAdapter;
import kr.co.palmstudio.palmstudio.util.PhotoDialog;
import kr.co.palmstudio.palmstudio.util.Util;
import kr.co.palmstudio.palmstudio.utils.Utils;

/**
 * Created by Administrator on 2016-01-11.
 */
public class OrderListActivity extends Activity implements View.OnClickListener
{
    public static String FOLDER_NAME = "palmstudio";
    ListViewAdapter adapter;
    ArrayList<OrderItem> orderList ;
    private AQuery m_aq;

    private PhotoDialog photoDialog ;
    private CommonDialog commonDialog ;

    private int press_photo_btn_id, press_photo_btn_position ;
    private String tmp_photo_file_path ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        m_aq = new AQuery(this);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(R.string.activity_order_title) ;

        textView = (TextView) findViewById(R.id.list_empty_tv) ;
        textView.setText(R.string.empty_order) ;

        photoDialog = new PhotoDialog(this) ;
        photoDialog.findViewById(R.id.btn_take_photo).setOnClickListener(this);
        photoDialog.findViewById(R.id.btn_select_photo).setOnClickListener(this);

        commonDialog = new CommonDialog(this, R.layout.dialog_layout_photo_view) ;
        commonDialog.findViewById(R.id.btn_download).setOnClickListener(this);

        orderList = new ArrayList<OrderItem>();

        findViewById(R.id.btn_back).setOnClickListener(this);

        Member.getOrder().getOrderList(this, this, "recvOrderList");
    }

    public void recvOrderList(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                if ( result )
                {
                    JSONObject jsonObject = _response.getJSONObject("order_list") ;
                    Iterator iterator = jsonObject.keys() ;

                    while(iterator.hasNext())
                    {
                        String key = (String) iterator.next() ;

                        if ( !key.equals("session_result") && !key.equals("is_login") && !key.equals("result") && !key.equals("reason") )
                        {
                            JSONObject jsonObject1 = jsonObject.getJSONObject(key) ;
                            orderList.add(new OrderItem(jsonObject1.optString("por_id"), jsonObject1.optString("por_state"), jsonObject1.optString("por_type"), jsonObject1.optString("por_num_of_photo"),
                                    jsonObject1.optString("por_edit_intensity"), jsonObject1.optString("por_background"), jsonObject1.optString("por_cloth"), jsonObject1.optString("por_orderer"), jsonObject1.optString("por_hp"),
                                    jsonObject1.optString("por_zip"), jsonObject1.optString("por_addr1"), jsonObject1.optString("por_addr2"), jsonObject1.optString("por_basic_amount"), jsonObject1.optString("por_delevery_amount"),
                                    jsonObject1.optString("por_date"), jsonObject1.optString("por_photo_thumb1"), jsonObject1.optString("por_photo_thumb2"), jsonObject1.optString("por_photo_thumb3"),
                                    jsonObject1.optString("por_note"), jsonObject1.optString("por_completion_img"), jsonObject1.optString("P_TYPE"), jsonObject1.optString("por_pay_state"), jsonObject1.optString("P_VACT_NUM"),
                                    jsonObject1.optString("P_VACT_DATE"), jsonObject1.optString("P_VACT_NAME"), jsonObject1.optString("P_VACT_BANK_CODE") ));


                        }
                    }

                    adapter = new ListViewAdapter(this, orderList, R.layout.item_order, onClickListener);

                    final ListView listView = (ListView) findViewById(R.id.listView);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                            adapter.ToggleContents(listView, position);
                        }
                    });

                    if ( orderList.size() > 0 )
                    {
                        TextView textView = (TextView) findViewById(R.id.list_empty_tv) ;
                        textView.setVisibility(View.INVISIBLE);
                    }
                }
                else
                {
                    //successful ajax call, show status code and json content
                    //Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }
            Log.i("Notification", _response.toString());
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            onOrderItemClick(view) ;
        }
    } ;

    protected void onActivityResult (int requestCode, int resultCode, Intent receivedIntent)
    {
        if (resultCode == RESULT_CANCELED)
            return;

        if (requestCode == C.Preference.ACT_SELECT_BG)
        {
            OrderItem orderItem = orderList.get(adapter.getSelected_img_bg()) ;

            String selected_photo_bg = Integer.toString(receivedIntent.getIntExtra("BG", 1) + 1) ;
            orderItem.setPhoto_bg(selected_photo_bg);

            adapter.notifyDataSetChanged();
        }
        else if (requestCode == C.Preference.ACT_TAKE_PHOTO || requestCode == C.Preference.ACT_SELECT_PHOTO)
        {
            if ( requestCode == C.Preference.ACT_SELECT_PHOTO )
                tmp_photo_file_path = getImagePath(receivedIntent.getData());

            OrderItem orderItem = orderList.get(press_photo_btn_position) ;

            if ( press_photo_btn_id == R.id.img_photo1 ) {
                orderItem.setUpload_img1(tmp_photo_file_path);
                orderItem.setbLocal_img1(true);
            }
            else if ( press_photo_btn_id == R.id.img_photo2 ) {
                orderItem.setUpload_img2(tmp_photo_file_path);
                orderItem.setbLocal_img2(true);
            }
            else if ( press_photo_btn_id == R.id.img_photo3 ) {
                orderItem.setUpload_img3(tmp_photo_file_path);
                orderItem.setbLocal_img3(true);
            }

            orderList.set(press_photo_btn_position, orderItem) ;

            adapter.setData(orderList);

            Log.d("Selected the photo", tmp_photo_file_path);
        }
    }

    @Override
    public void onClick(View view)
    {
        int btn_id = view.getId() ;

        if ( btn_id == R.id.btn_back )
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if ( btn_id == R.id.btn_take_photo )
        {
            photoDialog.dismiss();

            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), C.Preference.TEMP_DIR);

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    // Log.i("", "failed to create directory");
                    return ;
                }
            }

            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

            String FILE_PATH = mediaFile.getAbsolutePath();

            // Set file path into single ton way
            Uri mUri = Uri.parse(FILE_PATH);

            tmp_photo_file_path = mUri.toString();

            MediaScannerConnection.scanFile(this,
                    new String[]{FILE_PATH}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                        }
                    });

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE) ;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile)) ;
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

            if ( intent.resolveActivity(getPackageManager()) != null )
            {
                startActivityForResult(intent, C.Preference.ACT_TAKE_PHOTO) ;
            }
        }
        else if ( btn_id == R.id.btn_select_photo )
        {
            photoDialog.dismiss();

            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
            intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, C.Preference.ACT_SELECT_PHOTO);
        }
        else if ( btn_id == R.id.btn_download )
        {
            final String url = commonDialog.getPhotoUrl() ;
            final File file = getOutputMediaFile(this);

            m_aq.download(url, file, new AjaxCallback<File>() {
                @Override
                public void callback(String url, File object, AjaxStatus status) {
                    if (object != null) {
                        Toast.makeText(OrderListActivity.this, "이미지 다운로드를 성공했습니다.", Toast.LENGTH_SHORT).show();
                        commonDialog.dismiss();

                        // Refresh Gallery
                        Utils.addPictureToGallery(OrderListActivity.this, file.getAbsolutePath());
                    }
                    else {
                        Toast.makeText(OrderListActivity.this, "이미지 다운로드를 실패했습니다.\n잠시후 다시 시도해 주세요", Toast.LENGTH_SHORT).show();
                        commonDialog.dismiss();
                    }
                }
            });
        }
    }

    private void onOrderItemClick(View view)
    {
        int btn_id = view.getId() ;

        if ( btn_id == R.id.img_photo1 || btn_id == R.id.img_photo2 || btn_id == R.id.img_photo3 )
        {
            press_photo_btn_position = Integer.parseInt(view.getTag().toString()) ;
            press_photo_btn_id = btn_id ;
            photoDialog.show();
        }
        else if ( view.getId() == R.id.btn_download_result )
        {
            commonDialog.setPhoto(view.getTag().toString());
            commonDialog.show();
        }
        else if ( view.getId() == R.id.btn_upload_img )
        {
            final String tag = view.getTag().toString();
            String[] temp;
            String delimiter = "\\|";
            temp = tag.split(delimiter);

            Log.d("UPLOAD", "upload modified img");

            switch( temp.length )
            {
                case 1:
                {
                    Toast.makeText(OrderListActivity.this, "수정된 이미지가 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
                case 2:
                {
                    Member.getOrder().modify_photo(this, this, "recvOrderResult", temp[0], temp[1], "", "" );
                }
                break;
                case 3:
                {
                    Member.getOrder().modify_photo(this, this, "recvOrderResult", temp[0], temp[1], temp[2], "" );
                }
                break;
                case 4:
                {
                    Member.getOrder().modify_photo(this, this, "recvOrderResult", temp[0], temp[1], temp[2], temp[3] );
                }
                break;
            }
        }
    }

    public void recvOrderResult(int _statusCode, JSONObject _response)
    {
        Log.i("Order", "Result");

        if(_response != null)
        {
            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                //successful ajax call, show status code and json content
                //Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                //CommonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                if( result )
                {
                    AlertDialog.Builder ab = new AlertDialog.Builder( this );
                    ab.setMessage(reason);
                    ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getApplicationContext(), OrderListActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });

                    ab.setTitle("주문 완료");
                    ab.show();

                }
                else
                {
                    CommonToast toast ;
                    toast = new CommonToast(getApplicationContext()) ;
                    toast.showToast(reason, Toast.LENGTH_LONG);
                }

            } catch (JSONException e) {
                e.printStackTrace();

                //ajax error, show error code
                Toast.makeText(getApplicationContext(), "Error:" + e.toString(), Toast.LENGTH_LONG).show();
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    public String getImagePath(Uri data)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);

        return imgPath;
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(Context mContext) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), FOLDER_NAME);

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.i("", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = null;
        // Photo mode
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "Download_" + timeStamp + ".jpg");

        String FILE_PATH = mediaFile.getAbsolutePath();

        // Set file path into single ton way
        Uri mUri = Uri.parse(FILE_PATH);

        CameraActivity.camera.setFilePath(mUri.toString());

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext,
                new String[]{FILE_PATH}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
        return mediaFile;
    }
}
