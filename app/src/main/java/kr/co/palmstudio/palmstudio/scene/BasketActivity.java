package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by Administrator on 2016-01-11.
 */
public class BasketActivity extends Activity implements View.OnClickListener
{
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(R.string.activity_basket_title) ;

        textView = (TextView) findViewById(R.id.list_empty_tv) ;
        textView.setText(R.string.empty_basket) ;

        findViewById(R.id.btn_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;

        }
    }
}
