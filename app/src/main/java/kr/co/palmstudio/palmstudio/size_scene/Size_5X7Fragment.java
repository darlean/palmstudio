package kr.co.palmstudio.palmstudio.size_scene;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.util.ImageUtil;

/**
 * Created by darlean on 2016. 5. 6..
 */
public class Size_5x7Fragment extends Fragment implements View.OnClickListener
{
    private OnClickListener listener;

    public interface OnClickListener {
        public void onSelectTypeClick(View v, int selType, String strType );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_size_5x7, container, false);

        rootView.findViewById(R.id.btn_size_5x7_1).setOnClickListener(this);
        rootView.findViewById(R.id.btn_size_5x7_2).setOnClickListener(this);
        rootView.findViewById(R.id.btn_size_5x7_3).setOnClickListener(this);

        int value = (ImageUtil.GetDisplayWidth(getActivity()) - 60) / 2 ;

        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.btn_size_5x7_1) ;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageButton.getLayoutParams();
        layoutParams.width = value ;
        layoutParams.height = value ;

        rootView.findViewById(R.id.btn_size_5x7_2).setLayoutParams(layoutParams);
        rootView.findViewById(R.id.btn_size_5x7_3).setLayoutParams(layoutParams);
        rootView.findViewById(R.id.btn_size_5x7_4).setLayoutParams(layoutParams);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = (OnClickListener) activity ;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement Size_5x7Fragment.OnClickListener");
        }
    }

    @Override
    public void onClick(View button)
    {
        int btnID = button.getId() ;
        int selType = -1 ;
        String strType = "" ;

        if ( btnID == R.id.btn_size_5x7_1 )
        {
            selType = C.Preference.SIZE_5x7_1 ;
            strType = "5cmx7cm 2매, 2.5cmx3cm 3매" ;
        }
        else if ( btnID == R.id.btn_size_5x7_2 )
        {
            selType = C.Preference.SIZE_5x7_2 ;
            strType = "5cmx7cm 2매, 3.5cmx4.5cm 2매" ;
        }
        else if ( btnID == R.id.btn_size_5x7_3 )
        {
            selType = C.Preference.SIZE_5x7_3 ;
            strType = "5cmx7cm 2매, 3cmx4cm 2매" ;
        }

        listener.onSelectTypeClick(button, selType, strType);
    }
}
