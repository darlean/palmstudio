package kr.co.palmstudio.palmstudio.helper;

/**
 * Created by Administrator on 2015-10-12.
 */
public interface AsyncCallback<T>
{
    public static abstract class Base<T> implements AsyncCallback<T> {

        @Override
        public void exceptionOccured(Exception e) {
        }

        @Override
        public void cancelled() {
        }

    }

    public void onResult(T result);

    public void exceptionOccured(Exception e);

    public void cancelled();
}
