package kr.co.palmstudio.palmstudio.member;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.helper.HttpClientHelper;

/**
 * Created by 홍군 on 2016-02-12.
 */
public class Member
{
    private String                  m_device_id;            // 디바이스 ID
    private String                  m_session_id;           // 세션 ID

    private String                  m_memUserID = null;     // ID
    private String                  m_aulKey = null;        // aul key
    private String                  m_Name = null;     // ID
    private String                  m_HP = null;        // Hp
    private String                  m_Zip = null;        // aul key
    private String                  m_Addr1 = null;     // ID
    private String                  m_Addr2 = null;        // aul key

    private MemberSession           m_session = new MemberSession();
    private MemberUsage             m_usage = new MemberUsage(this);
    private MemberOrder             m_order = new MemberOrder();


    // 싱글톤을 위해 static 으로 선언
    private volatile static Member  s_instance = null;

    private Member()
    {

    }

    // 싱글톤
    public static Member getInstance()
    {
        if(s_instance == null)
        {
            synchronized(Member.class)
            {
                if(s_instance == null) {
                    s_instance = new Member();
                }
            }
        }
        return s_instance;
    }

    // obtain session
    public static MemberSession getSession()   { return getInstance().m_session; }

    // obtain usage
    public static MemberUsage getUsage()       { return getInstance().m_usage; }

    // obtain order
    public static MemberOrder getOrder()        { return getInstance().m_order; }

    // is login
    public boolean isLogin()                   { return getInstance().getMemUserID() != null &&  getInstance().getMemUserID().length() > 0; }

    // is auto login
    public boolean isAutoLogin()                { return isLogin() && getInstance().getAulKey() != null &&  getInstance().getAulKey().length() > 0; }

    // clear
    public void clear()
    {
        m_device_id     = null;             // 디바이스 ID
        m_session_id    = null;             // 세션 ID
        m_memUserID     = null;             // ID
        m_aulKey        = null;             // aul key
    }

    // remove cookies
    public void logout( Context _ctx )
    {
        /*AsyncHttpClient client = HttpClientHelper.getInstance();
        PersistentCookieStore myCookieStore = new PersistentCookieStore(_ctx);
        client.setCookieStore(myCookieStore);

        myCookieStore.clear();*/

        clear();
    }

    // 멤버 초기화
    public void init( Context _ctx )
    {
        SharedPreferences setting = _ctx.getSharedPreferences(C.Preference.PREFERENCES_KEY, 0);

        String AUL_KEY = setting.getString(C.Preference.AUL_KEY, "");
        String MEM_ID = setting.getString(C.Preference.MEMBER_USERID, "");

        String mbName = setting.getString(C.Preference.MEMBER_NAME, "");
        String mbHP = setting.getString(C.Preference.MEMBER_HP, "");
        String mbZip = setting.getString(C.Preference.MEMBER_ZIP, "");
        String mbAddr1 = setting.getString(C.Preference.MEMBER_ADDR1, "");
        String mbAddr2 = setting.getString(C.Preference.MEMBER_ADDR2, "");

        if( AUL_KEY.length() > 0  && MEM_ID.length() > 0 )
        {
            //Toast.makeText(_ctx, AUL_KEY + " " + MEM_ID, Toast.LENGTH_LONG).show();
            Log.i("AutoLogin", AUL_KEY + " " + MEM_ID );
            setAulKey(AUL_KEY);
            setMemUserID(MEM_ID);

            setName(mbName);
            setHP(mbHP);
            setZip(mbZip);
            setAddr1(mbAddr1);
            setAddr2(mbAddr2);
        }



        // 자동 로그인 체크
        /*String aulKey = getSession().findValueFromCookies( myCookieStore.getCookies(), C.Preference.AUL_KEY);
        if( aulKey == null )
        {
            getUsage().debugCookie(myCookieStore);

            // 쿠키를 삭제하고 다시 설정한다.
            myCookieStore.clear();

            getUsage().debugCookie(myCookieStore);
            //myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
            //myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_USERID, "") );

            return;
        }

        // 유저 아이디가 없을 경우에도 다시 설정
        String mbID = getSession().findValueFromCookies( myCookieStore.getCookies(), C.Preference.MEMBER_USERID);
        // 이메일
        //String email = getSession().findValueFromCookies( myCookieStore.getCookies(), C.Preference.MEMBER_EMAIL);
        // 전화번호
        //String phone = getSession().findValueFromCookies( myCookieStore.getCookies(), C.Preference.MEMBER_PHONE);

        if( mbID != null )//&& email != null && phone != null )
        {
            // 일단 삭제한다.
            //myCookieStore.clear();

            setAulKey(aulKey);
            setMemUserID(mbID);

            //myCookieStore.addCookie(new BasicClientCookie(C.Preference.AUL_KEY, aulKey));
            //myCookieStore.addCookie(new BasicClientCookie(C.Preference.MEMBER_USERID, mbID));
        }
        else
        {
            // 하나라도 값이 없다면
            // 쿠키를 삭제하고 다시 설정한다.
            //myCookieStore.clear();
            myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.AUL_KEY, "") );
            myCookieStore.deleteCookie( new BasicClientCookie(C.Preference.MEMBER_USERID, "") );
        }

        client.setCookieStore(myCookieStore);
        getUsage().debugCookie(myCookieStore);*/
    }


    //
    // set / get method
    //
    public String getDevice_id() {
        return m_device_id;
    }

    public void setDevice_id(String m_device_id) {
        this.m_device_id = m_device_id;
    }
    public String getSession_id() {
        return m_session_id;
    }
    public void setSession_id(String m_session_id) {
        this.m_session_id = m_session_id;
    }

    public String getMemUserID() {        return m_memUserID;    }
    public void setMemUserID(String m_memUserID) {        this.m_memUserID = m_memUserID;    }
    public String getAulKey() {        return m_aulKey;    }
    public void setAulKey(String m_aulKey) {        this.m_aulKey = m_aulKey;    }

    public String getName() {
        return m_Name;
    }

    public void setName(String m_Name) {
        this.m_Name = m_Name;
    }

    public String getZip() {
        return m_Zip;
    }

    public void setZip(String m_Zip) {
        this.m_Zip = m_Zip;
    }

    public String getAddr1() {
        return m_Addr1;
    }

    public void setAddr1(String m_Addr1) {
        this.m_Addr1 = m_Addr1;
    }

    public String getAddr2() {
        return m_Addr2;
    }

    public void setAddr2(String m_Addr2) {
        this.m_Addr2 = m_Addr2;
    }

    public String getHP() {
        return m_HP;
    }

    public void setHP(String m_HP) {
        this.m_HP = m_HP;
    }
}
