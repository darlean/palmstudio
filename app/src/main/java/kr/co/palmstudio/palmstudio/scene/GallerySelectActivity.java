package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.ImageAdapter;

/**
 * Created by Administrator on 2016-01-14.
 */
public class GallerySelectActivity extends Activity implements View.OnClickListener
{
    CommonToast toast ;

    ImageAdapter myImageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_select);

        // Get memory class of this device, exceeding this amount will throw an
        // OutOfMemory exception.
        final int memClass
                = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();

        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View.OnClickListener onCheckBoxClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                selectImage(v.getId());
            }
        } ;

        myImageAdapter = new ImageAdapter(this, memClass, "/DCIM/Camera", mInflater, true, onCheckBoxClickListener);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(myImageAdapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                selectImage(position);
            }
        });

        toast = new CommonToast(getApplicationContext()) ;

        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.select_all).setOnClickListener(this);
        findViewById(R.id.btn_delete).setOnClickListener(this);
        findViewById(R.id.btn_order).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId() ;

        if ( id == R.id.btn_close )
        {
            Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
            startActivity(intent);
            finish();
        }
        else if ( id == R.id.select_all )
        {
            CheckBox checkBox = (CheckBox) view ;

            myImageAdapter.selectAll(checkBox.isChecked());
            myImageAdapter.notifyDataSetChanged();
            setTitle();
        }
        else if ( id == R.id.btn_delete )
        {
            if ( myImageAdapter.getSelectedCount() == 0 )
            {
                toast.showToast("삭제 할 사진을 선택해 주세요.", Toast.LENGTH_LONG) ;
            }
        }
        else if ( id == R.id.btn_order )
        {
            if ( myImageAdapter.getSelectedCount() == 0 || myImageAdapter.getSelectedCount() > 1 )
            {
                toast.showToast("주문하실 사진을 한 장만 선택해 주세요.", Toast.LENGTH_LONG);
            }
        }
    }

    public final void selectImage(int selectedIndex)
    {
        myImageAdapter.setCheckBox(selectedIndex);
        myImageAdapter.notifyDataSetChanged();
        setTitle();
    }

    public void setTitle()
    {
        TextView textView = (TextView) findViewById(R.id.tv_title) ;

        int nCnt = myImageAdapter.getSelectedCount() ;

        if ( nCnt == 0 )
            textView.setText( "사진 선택" )  ;
        else
            textView.setText( nCnt + "장 선택") ;
    }
}
