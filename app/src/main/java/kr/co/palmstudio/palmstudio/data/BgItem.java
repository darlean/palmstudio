package kr.co.palmstudio.palmstudio.data;

/**
 * Created by darlean on 2016. 3. 2..
 */
public class BgItem
{
    private int BG_ID ;
    private String BG_NAME ;

    public int getBgID()
    {
        return this.BG_ID ;
    }
    public String getBgName() { return this.BG_NAME; }

    public BgItem(int BG_ID, String BG_NAME)
    {
        this.BG_ID = BG_ID ;
        this.BG_NAME = BG_NAME ;
    }
}
