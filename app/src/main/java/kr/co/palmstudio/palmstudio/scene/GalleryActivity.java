package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.ImageAdapter;

/**
 * Created by Administrator on 2016-01-13.
 */
public class GalleryActivity extends Activity implements View.OnClickListener
{
    ImageAdapter myImageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        // Get memory class of this device, exceeding this amount will throw an
        // OutOfMemory exception.
        final int memClass
                = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();

        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        myImageAdapter = new ImageAdapter(this, memClass, "/DCIM/Camera", mInflater, false, null);

        gridview.setAdapter(myImageAdapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                callImageViewer(position);
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId() ;

        if ( id == R.id.btn_back )
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if ( id == R.id.btn_select )
        {
            Intent intent = new Intent(getApplicationContext(), GallerySelectActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    public final void callImageViewer(int selectedIndex){
        Intent i = new Intent(this, GalleryBigActivity.class);
        String imgPath = myImageAdapter.get(selectedIndex) ;
        i.putExtra("filename", imgPath);
        startActivityForResult(i, 1);
    }
}
