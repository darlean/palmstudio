package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by darlean on 2016. 5. 14..
 */
public class TipActivity extends Activity implements View.OnClickListener
{
    private ViewPager mViewPager ;
    private ImageAdapter adapter ;
    private List<ImageView> dots ;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE) ;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.activity_guide);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);

        adapter = new ImageAdapter(this);
        mViewPager.setAdapter(adapter);

        findViewById(R.id.btn_close).setOnClickListener(this);

        addDots() ;
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_close:
                finish();
                break ;
        }
    }

    public void addDots()
    {
        dots = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout)findViewById(R.id.dots);

        for(int i = 0; i < adapter.getCount() ; i++) {
            ImageView dot = new ImageView(this);
            dot.setAdjustViewBounds(true);

            if ( i == 0 )
            {
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_selected));
            }
            else
            {
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_not_selected));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.leftMargin = 5 ;
            dotsLayout.addView(dot, params);

            dots.add(dot);
        }

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void selectDot(int idx)
    {
        Resources res = getResources();
        for(int i = 0; i < adapter.getCount() ; i++) {
            int drawableId = (i==idx)?(R.drawable.pager_dot_selected):(R.drawable.pager_dot_not_selected);
            Drawable drawable = res.getDrawable(drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }

    public class ImageAdapter extends PagerAdapter
    {
        Context context;

        private int[] GalImages = new int[] {
                R.drawable.tip1,
                R.drawable.tip2,
                R.drawable.tip3,
                R.drawable.tip4,
        };

        ImageAdapter(Context context)
        {
            this.context=context;
        }

        @Override
        public int getCount()
        {
            return GalImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object)
        {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            ImageView imageView = new ImageView(context);
            int padding = 10 ;//context.getResources().getDimensionPixelSize(R.dimen.padding_medium);

            imageView.setPadding(padding, padding, padding, padding);

            //imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            imageView.setImageResource(GalImages[position]);

            ((ViewPager) container).addView(imageView, 0);

            return imageView;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            ((ViewPager) container).removeView((ImageView) object);
        }

    }
}
