package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.ImageUtil;

/**
 * Created by Administrator on 2016-01-13.
 */
public class GalleryActivity2 extends Activity {
    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        ArrayList<String> itemList = new ArrayList<String>();

        public ImageAdapter(Context c) {
            mContext = c;
        }

        void add(String path){
            itemList.add(path);
        }

        @Override
        public int getCount() {
            return itemList.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                //imageView.setLayoutParams(new GridView.LayoutParams(220, 220));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                //imageView.setPadding(1, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            Bitmap bm = ImageUtil.loadFixedSizeBitmap(itemList.get(position), 119, 119);

            imageView.setImageBitmap(bm);
            return imageView;
        }

        /*public Bitmap SafeDecodeBitmapFile(String strFilePath)
        {
            //DEBUG.SHOW_DEBUG(TAG, "[ImageDownloader] SafeDecodeBitmapFile : " + strFilePath);

            try
            {
                File file = new File(strFilePath);
                if (file.exists() == false)
                {
                    //DEBUG.SHOW_ERROR(TAG, "[ImageDownloader] SafeDecodeBitmapFile : File does not exist !!");

                    return null;
                }

                // Max image size
                final int IMAGE_MAX_SIZE 	= GlobalConstants.getMaxImagePixelSize();
                BitmapFactory.Options bfo 	= new BitmapFactory.Options();
                bfo.inJustDecodeBounds 		= true;

                BitmapFactory.decodeFile(strFilePath, bfo);

                if(bfo.outHeight * bfo.outWidth >= IMAGE_MAX_SIZE * IMAGE_MAX_SIZE)
                {
                    bfo.inSampleSize = (int)Math.pow(2, (int)Math.round(Math.log(IMAGE_MAX_SIZE
                            / (double) Math.max(bfo.outHeight, bfo.outWidth)) / Math.log(0.5)));
                }
                bfo.inJustDecodeBounds = false;
                bfo.inPurgeable = true;
                bfo.inDither = true;

                final Bitmap bitmap = BitmapFactory.decodeFile(strFilePath, bfo);

                int degree = GetExifOrientation(strFilePath);

                return GetRotatedBitmap(bitmap, degree);
            }
            catch(OutOfMemoryError ex)
            {
                ex.printStackTrace();

                return null;
            }
        }*/
    }

    ImageAdapter myImageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        myImageAdapter = new ImageAdapter(this);
        gridview.setAdapter(myImageAdapter);

        String ExternalStorageDirectoryPath = Environment
                .getExternalStorageDirectory()
                .getAbsolutePath();

        String targetPath = ExternalStorageDirectoryPath + "/DCIM/Camera" ;

        Toast.makeText(getApplicationContext(), targetPath, Toast.LENGTH_LONG).show();
        File targetDirector = new File(targetPath);

        File[] files = targetDirector.listFiles();
        for (File file : files){
            myImageAdapter.add(file.getAbsolutePath());
        }
    }
}
