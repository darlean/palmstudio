package kr.co.palmstudio.palmstudio.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by Administrator on 2016-01-15.
 */

public class ImageAdapter extends BaseAdapter
{
    private LayoutInflater mInflater;
    private LruCache<String, Bitmap> mMemoryCache;
    private int mCellWidth = 200 ;
    private Context mContext;
    private boolean mShowCheckBox = false ;
    private View.OnClickListener mOnClickListner = null ;

    ArrayList<String> itemList = new ArrayList<String>();
    boolean[] thumbnailsselection ;

    public ImageAdapter(Context c, int memClass, String subPath, LayoutInflater in_inflater, boolean bShowCheckBox, View.OnClickListener onClickListener)
    {
        mContext = c ;
        mInflater = in_inflater ;
        mShowCheckBox = bShowCheckBox ;
        mOnClickListner = onClickListener ;

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = 1024 * 1024 * memClass / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {

            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in bytes rather than number of items.
                return bitmap.getByteCount();
            }
        };

        int displayWidhth = ImageUtil.GetDisplayWidth(c) ;
        mCellWidth = (displayWidhth - 2 ) / 3 ;

        String ExternalStorageDirectoryPath = Environment
                .getExternalStorageDirectory()
                .getAbsolutePath();

        String targetPath = ExternalStorageDirectoryPath + subPath ;

        //Toast.makeText(getApplicationContext(), targetPath, Toast.LENGTH_LONG).show();
        File targetDirector = new File(targetPath);

        File[] files = targetDirector.listFiles();
        for (File file : files)
        {
            add(file.getAbsolutePath());
        }

        thumbnailsselection = new boolean[getCount()] ;
    }

    public void add(String path){
        itemList.add(path);
    }

    public String get(int position)
    {
        return itemList.get(position) ;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.item_gallery, null);
            holder.imageview = (ImageView) convertView.findViewById(R.id.thumbImage);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.itemCheckBox);

            holder.imageview.setLayoutParams(new RelativeLayout.LayoutParams(mCellWidth, mCellWidth));
            holder.imageview.setAdjustViewBounds(true);
            holder.imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ( thumbnailsselection[position] )
        {
            holder.checkbox.setChecked(true);
            holder.imageview.setColorFilter(0x66000000);
        }
        else
        {
            holder.checkbox.setChecked(false);
            holder.imageview.setColorFilter(0x00000000);
        }

        holder.checkbox.setId(position);
        holder.imageview.setId(position);

        if ( mShowCheckBox == false )
        {
            holder.checkbox.setVisibility(View.GONE);
        }

        // Use the path as the key to LruCache
        final String imageKey = itemList.get(position);
        final Bitmap bm = getBitmapFromMemCache(imageKey);

        if (bm == null){
            BitmapWorkerTask task = new BitmapWorkerTask(holder.imageview);
            task.execute(imageKey);
        };

        holder.imageview.setImageBitmap(bm);

        if ( mOnClickListner != null )
        {
            holder.checkbox.setOnClickListener(mOnClickListner) ;
        }

        holder.id = position;
        return convertView;
    }

    class ViewHolder
    {
        ImageView imageview;
        CheckBox checkbox;
        int id;

        public ImageView getImageView()
        {
            return imageview ;
        }
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap>
    {
        private final WeakReference<ImageView> imageViewReference;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params)
        {
            final Bitmap bitmap = ImageUtil.loadFixedSizeBitmap(params[0], mCellWidth, mCellWidth) ;

            addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = (ImageView)imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return (Bitmap) mMemoryCache.get(key);
    }

    public void setCheckBox(int position)
    {
        thumbnailsselection[position] = !thumbnailsselection[position] ;
    }

    public void selectAll(boolean bSelect)
    {
        for (int i = 0; i < getCount() ; i++)
        {
            thumbnailsselection[i] = bSelect ;
        }
    }

    public int getSelectedCount()
    {
        int nCnt = 0 ;

        for (int i = 0; i < getCount() ; i++)
        {
            if ( thumbnailsselection[i] ) nCnt++ ;
        }

        return nCnt ;
    }

    public boolean getChecked(int position)
    {
        return thumbnailsselection[position] ;
    }
}
