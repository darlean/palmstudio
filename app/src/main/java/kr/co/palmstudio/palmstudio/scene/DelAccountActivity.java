package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-20.
 */
public class DelAccountActivity extends Activity implements View.OnClickListener
{
    private CheckBox m_chkAgree;
    private AQuery m_aq;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del_account);

        m_aq = new AQuery(this);

        TextView tv_title = (TextView) findViewById(R.id.tv_title) ;
        tv_title.setText(R.string.activity_del_account_title) ;

        m_chkAgree = m_aq.find(R.id.cb_auto_login ).getCheckBox();

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_leave).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MyInfoActivity.class);
                startActivity(intent);
                finish();
                break ;
            case R.id.btn_leave:

                CheckBox agree = m_aq.find(R.id.chk_agree ).getCheckBox();
                if( !agree.isChecked() )
                {
                    Toast.makeText(this, "탈퇴 동의를 해주세요.", Toast.LENGTH_LONG).show();

                    return;
                }

                Member.getUsage().leave(this, this, "onLeave");

                break;
        }
    }

    public void onLeave(int _statusCode, JSONObject _response)
    {
        if( _response != null )
        {
            Log.i("Leave", _response.toString());

            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");
                if( result )
                {
                    // 로그 아웃
                    Member.getInstance().logout(this);

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    //successful ajax call, show status code and json content
                    Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }
            Log.i("Main", _response.toString());
        }


    }
}

