package kr.co.palmstudio.palmstudio.util;

import android.content.Context;

/**
 * Created by darlean on 2016. 1. 16..
 */
public class CItemData {

    private String m_szLabel;
    private int m_btnType ;
    private int m_btnID ;

    private boolean m_bContents ;

    public CItemData(Context context, String p_szLabel, int p_btnType, boolean p_bContents)
    {
        m_szLabel = p_szLabel;
        m_btnType = p_btnType;
        m_bContents = p_bContents;
    }

    public String getLabel()
    {
        return m_szLabel;
    }

    public int getBtnType()
    {
        return m_btnType ;
    }

    public boolean haveContents()
    {
        return m_bContents ;
    }

    public void setID(int btnID)
    {
        m_btnID = btnID ;
    }
}