package kr.co.palmstudio.palmstudio.member;

import android.content.Context;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by 홍군 on 2016-03-06.
 */
public class MemberOrder {
    class Option {
        String m_photo_type = "";       // 사진 종류
        int m_number_of_photo = 0;        // 출력 매수
        String m_edit_intensity = "";       // 보정 강도
        String m_memo = "";                  // 메모
        String m_background = "";            // 배경 이미지
        String m_cloth = "";                 // 의상 이미지

        public Option() {

        }

        public void setOption(String _photo_type, int _num_of_photo, String _edit_intensity, String _memo, String _background, String _cloth) {
            m_photo_type = _photo_type;
            m_number_of_photo = _num_of_photo;
            m_edit_intensity = _edit_intensity;
            m_memo = _memo;
            m_background = _background;
            m_cloth = _cloth;
        }
    }

    class Payment {
        String m_orderer = "";                     // 주문자
        String m_hp = "";                          // 휴대폰
        String m_zip = "";                         // 우편번호
        String m_addr1 = "";                       // 주소1
        String m_addr2 = "";                       // 주소2

        String m_delevery_type;                    // 배송 방법
        int m_order_amount = 0;                 // 주문 비용
        int m_delevery_amount = 0;              // 배송 비용
        int m_payment_type = 0;                 // 결제 방법

        Payment() {

        }

        public void setPayment(String _orderer, String _hp, String _zip, String _addr1, String _addr2, String _delevery_type, int _order_amount, int _delevery_amount, int _payment_type) {
            m_orderer = _orderer;
            m_hp = _hp;
            m_zip = _zip;
            m_addr1 = _addr1;
            m_addr2 = _addr2;
            m_delevery_type = _delevery_type;
            m_order_amount = _order_amount;
            m_delevery_amount = _delevery_amount;
            m_payment_type = _payment_type;

        }
    }

    String m_upload_img = null;                       // 업로드 이미지
    String m_upload_img2 = null;                      // 두번쨰 업로드 이미지
    String m_upload_img3 = null;                      // 세번째 업로드 이미지
    Option m_option = null;                           // 주문 옵션
    Payment m_payment = null;                          // 결제 정보


    public MemberOrder() {
        m_option = null;
    }

    public void setOption(String _photo_type, int _num_of_photo, String _edit_intensity, String _memo, String _background, String _cloth) {
        m_option = new Option();
        m_option.setOption(_photo_type, _num_of_photo, _edit_intensity, _memo, _background, _cloth);
    }

    public void setPayment(String _orderer, String _hp, String _zip, String _addr1, String _addr2, String _delevery_type, int _order_amount, int _delevery_amount, int _payment_type) {
        m_payment = new Payment();
        m_payment.setPayment(_orderer, _hp, _zip, _addr1, _addr2, _delevery_type, _order_amount, _delevery_amount, _payment_type);
    }

    public void setUploadImg(String _upload_img) {
        m_upload_img = new String();
        m_upload_img = _upload_img;
    }

    public void setUploadImg2(String _upload_img) {
        m_upload_img2 = new String();
        m_upload_img2 = _upload_img;
    }

    public void setUploadImg3(String _upload_img) {
        m_upload_img3 = new String();
        m_upload_img3 = _upload_img;
    }

    public String getUploadImg()    {        return m_upload_img ;    }
    public String getUploadImg2()   {        return m_upload_img2 ;    }
    public String getUploadImg3()   {        return m_upload_img3 ;    }

    public String getSelectedPhotoType()  { return m_option.m_photo_type; }
    public int    getNumOfPhoto() { return m_option.m_number_of_photo; }

    public void clear()
    {
        m_option = null;
        m_payment = null;
        m_upload_img = null;
        m_upload_img2 = null;
        m_upload_img3 = null;
    }

    public boolean insertOrder( Context _ctx, Object _handler, String _callback )
    {
        if( m_option == null ) return false;
        if( m_payment == null ) return false;
        if( m_upload_img == null ) return false;

        RequestParams params = new RequestParams();

        params.put("por_type", m_option.m_photo_type );
        params.put("por_num_of_photo", m_option.m_number_of_photo );
        params.put("por_edit_intensity", m_option.m_edit_intensity );
        params.put("por_memo", m_option.m_memo );
        params.put("por_background", m_option.m_background );
        params.put("por_cloth", m_option.m_cloth );

        params.put("por_orderer", m_payment.m_orderer);
        params.put("por_hp", m_payment.m_hp);
        params.put("por_zip", m_payment.m_zip);
        params.put("por_addr1", m_payment.m_addr1);
        params.put("por_addr2", m_payment.m_addr2);

        params.put("por_delevery_type", m_payment.m_delevery_type);
        params.put("por_basic_amount", m_payment.m_order_amount);
        params.put("por_delevery_amount", m_payment.m_delevery_amount);

        if( m_upload_img != null )
        {
            File myFile = new File(m_upload_img);
            try
            {
                params.put("por_upload_img", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }

        if( m_upload_img2 != null )
        {
            File myFile = new File(m_upload_img2);
            try
            {
                params.put("por_upload_img2", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }

        if( m_upload_img3 != null )
        {
            File myFile = new File(m_upload_img3);
            try
            {
                params.put("por_upload_img3", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }


        Member.getSession().post_with_dialog(_ctx, "주문 요청중 입니다.", "order/insert", params, _handler, _callback);
        return true;
    }

    public void getOrderList( Context _ctx, Object _handler, String _callback )
    {
        Member.getSession().post_with_dialog(_ctx, "주문 리스트를 수신중 입니다.", "order/order_lists", _handler, _callback);
    }


    public void modify_photo( Context _ctx, Object _handler, String _callback, String _id, String _img1, String _img2, String _img3 )
    {
        RequestParams params = new RequestParams();
        params.put("por_id", _id );

        if( _img1.length() > 0 )
        {
            File myFile = new File(_img1);
            try
            {
                params.put("por_upload_img", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }

        if( _img2.length() > 0 )
        {
            File myFile = new File(_img2);
            try
            {
                params.put("por_upload_img2", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }

        if( _img3.length() > 0 )
        {
            File myFile = new File(_img3);
            try
            {
                params.put("por_upload_img3", myFile);
            }
            catch(FileNotFoundException e)
            {}
        }

        Member.getSession().post_with_dialog(_ctx, "이미지 업로드 중입니다.", "order/modify_img", params, _handler, _callback);
    }
}
