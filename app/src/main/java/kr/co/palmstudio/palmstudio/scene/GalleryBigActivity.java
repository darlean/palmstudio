package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.ImageUtil;

/**
 * Created by Administrator on 2016-01-13.
 */

public class GalleryBigActivity extends Activity implements View.OnClickListener
{
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_big);

        /** 전송메시지 */
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        String imgPath = extras.getString("filename");

        int degree = ImageUtil.GetExifOrientation(imgPath) ;
        Bitmap resizeBitmap = ImageUtil.loadBackgroundBitmap(this, imgPath) ;
        Bitmap rotateBitmap = ImageUtil.GetRotatedBitmap(resizeBitmap, degree);

        ImageView iv = (ImageView)findViewById(R.id.imageView);
        iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setImageBitmap(rotateBitmap);
        resizeBitmap.recycle();

        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.btn_delete).setOnClickListener(this);
        findViewById(R.id.btn_order).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_close:
                Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
                startActivity(intent);
                finish();
                break ;

            case R.id.btn_delete:
                intent = new Intent(getApplicationContext(), GalleryActivity.class);
                startActivity(intent);
                finish();
                break ;

            case R.id.btn_order:
                intent = new Intent(getApplicationContext(), OrderListActivity.class);
                startActivity(intent);
                finish();
                break ;
        }
    }
}
