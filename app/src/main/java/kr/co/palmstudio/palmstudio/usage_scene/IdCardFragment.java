package kr.co.palmstudio.palmstudio.usage_scene;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.util.ImageUtil;

/**
 * Created by darlean on 2016. 5. 6..
 */
public class IdCardFragment extends Fragment implements View.OnClickListener
{
    private OnClickListener listener;

    public interface OnClickListener {
        public void onSelectTypeClick(View v, int selType, String strType );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_id_card, container, false);

        rootView.findViewById(R.id.btn_id_card_1).setOnClickListener(this);
        rootView.findViewById(R.id.btn_id_card_2).setOnClickListener(this);
        rootView.findViewById(R.id.btn_id_card_3).setOnClickListener(this);

        int value = (ImageUtil.GetDisplayWidth(getActivity()) - 60) / 2 ;

        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.btn_id_card_1) ;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageButton.getLayoutParams();
        layoutParams.width = value ;
        layoutParams.height = value ;

        rootView.findViewById(R.id.btn_id_card_2).setLayoutParams(layoutParams);
        rootView.findViewById(R.id.btn_id_card_3).setLayoutParams(layoutParams);
        rootView.findViewById(R.id.btn_id_card_4).setLayoutParams(layoutParams);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = (OnClickListener) activity ;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement IdCardFragment.OnClickListener");
        }
    }

    @Override
    public void onClick(View button)
    {
        int btnID = button.getId() ;
        int selType = -1 ;
        String strType = "" ;

        if ( btnID == R.id.btn_id_card_1 )
        {
            selType = C.Preference.ID_CARD_1 ;
            strType = "주민등록증 3cmx4cm 9매" ;
        }
        else if ( btnID == R.id.btn_id_card_2 )
        {
            selType = C.Preference.ID_CARD_2 ;
            strType = "주민등록증 3cmx4cm 4매, 3.5cmx4.5cm 4매" ;
        }
        else if ( btnID == R.id.btn_id_card_3 )
        {
            selType = C.Preference.ID_CARD_3 ;
            strType = "주민등록증 3cmx4cm 6매, 2.5cmx3cm 3매" ;
        }

        listener.onSelectTypeClick(button, selType, strType);
    }
}