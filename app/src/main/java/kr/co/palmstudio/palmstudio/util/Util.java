package kr.co.palmstudio.palmstudio.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.DecimalFormat;

/**
 * Created by darlean on 2016. 1. 26..
 */
public class Util
{
    public static void alertDlg(Context context, String title, String message, String btnStr)
    {
        AlertDialog.Builder dlg = new AlertDialog.Builder(context);
        dlg.setTitle(title) ;
        dlg.setMessage(message);
        dlg.setPositiveButton(btnStr, null) ;
        dlg.show() ;
    }

    public static void alertDlg(Context context, String message)
    {
        CommonDialog commonDialog = new CommonDialog(context, -1) ;
        commonDialog.setCancelable(false);
        commonDialog.setMessage(message) ;
        commonDialog.show();
    }

    public static void toast(Context context, String message)
    {
        CommonToast toast ;
        toast = new CommonToast(context) ;
        toast.showToast(message, Toast.LENGTH_LONG);
    }

    public static String getTextOfTextView(Context context, int id)
    {
        TextView textView = (TextView) ((Activity)context).findViewById(id);

        return textView.getText().toString() ;
    }

    public static String getTextOfEditText(Context context, int id)
    {
        EditText editText = (EditText) ((Activity)context).findViewById(id);

        return editText.getText().toString() ;
    }

    public static int getId(String resourceName, Class<?> c)
    {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + resourceName + " / " + c, e);
        }
    }

    public static String getDecimalFormat(String string)
    {
        DecimalFormat df = new DecimalFormat("#,##0");
        return df.format(Integer.parseInt(string));
    }

    public static String getDecimalFormat(int value)
    {
        DecimalFormat df = new DecimalFormat("#,##0");
        return df.format(value);
    }
}
