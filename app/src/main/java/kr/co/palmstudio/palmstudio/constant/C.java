package kr.co.palmstudio.palmstudio.constant;

/**
 * Created by 홍군 on 2016-02-08.
 * 상수 정의
 */
public final class C
{
    // 통신할 웹서버 주소
    public static final String BASE_URL = "http://palmstudio.co.kr/app/";
    public static final String BASE_ROOT_URL = "http://palmstudio.co.kr";
    public static String DOWNLOAD_URI = "market://details?id=kr.co.palmstudio.palmstudio";

    public final class Preference
    {
        public static final String TEMP_DIR = "palmstudio" ;

        public static final String NAME             = "palm_studio.co.kr";
        public static final String PREFERENCES_KEY  = "palm_settting";
        public static final String AUL_KEY          = "autologin";
        public static final String MEMBER_USERID    =  "member_id";
        public static final String MEMBER_NAME      =  "member_name";
        public static final String MEMBER_HP        =  "member_hp";
        public static final String MEMBER_ZIP       =  "member_zip";
        public static final String MEMBER_ADDR1     =  "member_addr1";
        public static final String MEMBER_ADDR2     =  "member_addr2";

        public static final String MEMBER_EMAIL     =  "member_email";
        public static final String MEMBER_PHONE     =  "member_phone";

        public static final String CI_SESSION       =  "ci_session";

        public static final String GCM_REGID = "gcm_regid";
        public static final String GCM_UPDATED = "gcm_updated";
        public static final String APP_VERSION = "app_version";
        public static final String DEVICEKEY = "devicekey";
        public static final String OPTIONKEY = "optionkey";

        public static final String LOCATION_SEARCH_RANGE = "location_range";
        public static final String LOCATION_TERM_AGREE = "location_term_agree";

        public final static int ACT_SELECT_BG = 1 ;
        public final static int ACT_FIND_ADDRESS = 2 ;
        public final static int ACT_SELECT_CLOTH = 3 ;
        public final static int ACT_TAKE_PHOTO = 4 ;
        public final static int ACT_SELECT_PHOTO = 5 ;
        public final static int ACT_INISYS_PAY = 6 ;

        public final static int BUSINESS_CARD_1 = 1 ;
        public final static int BUSINESS_CARD_2 = 2 ;
        public final static int BUSINESS_CARD_3 = 3 ;

        public final static int DRIVER_CARD_1 = 4 ;
        public final static int DRIVER_CARD_2 = 5 ;
        public final static int DRIVER_CARD_3 = 6 ;

        public final static int ID_CARD_1 = 7 ;
        public final static int ID_CARD_2 = 8 ;
        public final static int ID_CARD_3 = 9 ;

        public final static int ID_PHOTO_1 = 10 ;
        public final static int ID_PHOTO_2 = 11 ;
        public final static int ID_PHOTO_3 = 12 ;

        public final static int PASSPORT_1 = 13 ;
        public final static int PASSPORT_2 = 14 ;
        public final static int PASSPORT_3 = 15 ;

        public final static int RESUME_1 = 16 ;
        public final static int RESUME_2 = 17 ;
        public final static int RESUME_3 = 18 ;

        public final static int SAT_CARD_1 = 19 ;
        public final static int SAT_CARD_2 = 20 ;
        public final static int SAT_CARD_3 = 21 ;

        public final static int VISA_1 = 22 ;
        public final static int VISA_2 = 23 ;
        public final static int VISA_3 = 24 ;
        public final static int VISA_4 = 25 ;
        public final static int VISA_5 = 26 ;

        public final static int SIZE_2_5x3_1 = 27 ;
        public final static int SIZE_2_5x3_2 = 28 ;
        public final static int SIZE_2_5x3_3 = 29 ;

        public final static int SIZE_3_5x4_5_1 = 30 ;
        public final static int SIZE_3_5x4_5_2 = 31 ;
        public final static int SIZE_3_5x4_5_3 = 32 ;

        public final static int SIZE_3x4_1 = 33 ;
        public final static int SIZE_3x4_2 = 34 ;
        public final static int SIZE_3x4_3 = 35 ;

        public final static int SIZE_5x5_1 = 36 ;
        public final static int SIZE_5x5_2 = 37 ;
        public final static int SIZE_5x5_3 = 38 ;

        public final static int SIZE_5x7_1 = 39 ;
        public final static int SIZE_5x7_2 = 40 ;
        public final static int SIZE_5x7_3 = 41 ;
    }
}
