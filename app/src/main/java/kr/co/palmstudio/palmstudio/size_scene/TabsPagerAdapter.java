package kr.co.palmstudio.palmstudio.size_scene;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by darlean on 2016. 5. 6..
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.mNumOfTabs = 5;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new Size_2_5x3Fragment();
            case 1:
                return new Size_3x4Fragment();
            case 2:
                return new Size_3_5x4_5Fragment();
            case 3:
                return new Size_5x5Fragment();
            case 4:
                return new Size_5x7Fragment();
            default:
                return new Size_2_5x3Fragment();
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
