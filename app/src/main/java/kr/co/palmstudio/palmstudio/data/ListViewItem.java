package kr.co.palmstudio.palmstudio.data;

/**
 * Created by darlean on 2016. 1. 31..
 */
public class ListViewItem
{
    private String ITEM_ID ;
    private String ITEM_LABEL;

    public String getID(){return ITEM_ID;}

    public String getLabel(){return ITEM_LABEL;}

    public ListViewItem(String ITEM_ID, String ITEM_LABEL)
    {
        this.ITEM_ID = ITEM_ID ;
        this.ITEM_LABEL = ITEM_LABEL ;
    }
}