package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-19.
 */
public class FindPWActivity extends Activity implements View.OnClickListener
{
    AQuery              m_aq;
    EditText            m_editEmail;

    private CommonToast commonToast ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pw);

        m_aq = new AQuery(this);

        commonToast = new CommonToast(getApplicationContext()) ;

        TextView tv_title = (TextView) findViewById(R.id.tv_title) ;
        tv_title.setText(R.string.activity_find_pw_title) ;

        findViewById(R.id.btn_back).setOnClickListener(this);

        m_editEmail = (EditText)findViewById(R.id.edit_email);

        m_aq = new AQuery(this);
        m_aq.id(R.id.btn_find_pw).clicked(this, "findPassword");

        //m_editEmail.setText("test7@test.com");
    }

    public void findPassword(View view)
    {
        Log.i("Join", "confirmPhoneCert");

        Member.getUsage().findPassword( this, m_editEmail.getText().toString(), this, "recvFindPassword" );
    }

    public void recvFindPassword(int _statusCode, JSONObject _response)
    {
        if(_response != null){

            try {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                if( result )
                {

                }

                //successful ajax call, show status code and json content
                commonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
            }
            catch (JSONException e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Util.alertDlg(this, errors.toString());
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                break ;
        }
    }
}
