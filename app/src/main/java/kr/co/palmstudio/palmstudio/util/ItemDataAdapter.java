package kr.co.palmstudio.palmstudio.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by darlean on 2016. 1. 16..
 */
public class ItemDataAdapter extends ArrayAdapter<CItemData> implements View.OnClickListener, ListView.OnItemClickListener
{
    public interface OnMyItemClick {
        public void OnMyItemClick(int position);
    }
    private OnMyItemClick mOnMyItemClick ;

    public void setOnMyItemClick(OnMyItemClick onMyItemClick){
        this.mOnMyItemClick = onMyItemClick;
    }

    // 레이아웃 XML을 읽어들이기 위한 객체
    private Context m_context ;
    private LayoutInflater mInflater;
    private ListView m_listview ;

    public ItemDataAdapter(Context context, ArrayList<CItemData> object, ListView listview)
    {
        // 상위 클래스의 초기화 과정
        // context, 0, 자료구조
        super(context, 0, object);

        m_context = context ;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        m_listview = listview ;
    }

    // 보여지는 스타일을 자신이 만든 xml로 보이기 위한 구문
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder ;
        if (convertView == null)
        {
            holder = new ViewHolder();
            // XML 레이아웃을 직접 읽어서 리스트뷰에 넣음
            convertView = mInflater.inflate(R.layout.item_list, null);

            holder.tv = (TextView) convertView.findViewById(R.id.textView) ;

            holder.btn_next = (ImageButton) convertView.findViewById(R.id.btn_next) ;
            holder.btn_content_open = (ImageButton) convertView.findViewById(R.id.btn_content_open) ;
            holder.btn_content_close = (ImageButton) convertView.findViewById(R.id.btn_content_close) ;

            holder.btn_check = (CheckBox) convertView.findViewById(R.id.btn_check) ;

            holder.content = (RelativeLayout) convertView.findViewById(R.id.contents_layout) ;

            holder.btn_next.setOnClickListener(this) ;
            holder.btn_content_open.setOnClickListener(this) ;
            holder.btn_content_close.setOnClickListener(this) ;

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.btn_next.setId(position) ;
        holder.btn_content_open.setId(position) ;
        holder.btn_content_close.setId(position) ;

        final CItemData data = this.getItem(position);

        if (data != null)
        {
            holder.tv.setText(data.getLabel());
            holder.btn_type = data.getBtnType() ;

            if ( data.getBtnType() == 1 )
            {
                holder.btn_next.setVisibility(View.VISIBLE);
            }
            else if ( data.getBtnType() == 2 )
            {
                holder.btn_check.setVisibility(View.VISIBLE);
            }
            else if ( data.getBtnType() == 3 )
            {
                holder.btn_content_open.setVisibility(View.VISIBLE);
            }
        }

        data.setID(position) ;

        holder.id = position ;
        return convertView ;
    }

    public void basicBtnProcess(int position)
    {
        ViewHolder viewHolder = (ViewHolder) getViewByPosition(position, m_listview).getTag() ;

        if ( viewHolder != null && viewHolder.btn_type == 3 )
        {
            if ( viewHolder.btn_content_open.getVisibility() == View.VISIBLE )
            {
                viewHolder.btn_content_open.setVisibility(View.GONE);
                viewHolder.btn_content_close.setVisibility(View.VISIBLE);
                viewHolder.content.setVisibility(View.VISIBLE);
            }
            else
            {
                viewHolder.btn_content_close.setVisibility(View.GONE);
                viewHolder.btn_content_open.setVisibility(View.VISIBLE);
                viewHolder.content.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        basicBtnProcess(view.getId()) ;

        if(mOnMyItemClick != null)
            mOnMyItemClick.OnMyItemClick (view.getId()) ;
    }

    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
    {
        basicBtnProcess(position);

        if(mOnMyItemClick != null)
            mOnMyItemClick.OnMyItemClick (position) ;
    }

    public View getViewByPosition(int pos, ListView listView)
    {
        try {
            final int firstListItemPosition = listView
                    .getFirstVisiblePosition();
            final int lastListItemPosition = firstListItemPosition
                    + listView.getChildCount() - 1;

            if (pos < firstListItemPosition || pos > lastListItemPosition)
            {
                return listView.getAdapter().getView(pos, null, listView);
            }
            else
            {
                final int childIndex = pos - firstListItemPosition;
                return listView.getChildAt(childIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    class ViewHolder
    {
        TextView    tv ;

        ImageButton btn_next ;
        ImageButton btn_content_open ;
        ImageButton btn_content_close ;

        CheckBox    btn_check ;

        RelativeLayout content ;

        int         id ;
        int         btn_type ;
    }
}
