package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import kr.co.palmstudio.palmstudio.R;

/**
 * Created by Administrator on 2016-01-11.
 */
public class NoticePopUpActivity extends Activity implements View.OnClickListener
{
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_with_webview_layout);

        findViewById(R.id.btn_ok).setOnClickListener(this);

        processIntent();
    }

    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
        processIntent();
    }

    private void processIntent()
    {
        Intent receivedIntent = getIntent() ;

        if ( receivedIntent == null )
            return ;

        String Url = receivedIntent.getExtras().getString("URL") ;

        if ( !Url.equals("") )
        {
            setHTMLText(Url) ;
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_ok:
                finish();
                break ;

        }
    }

    public void setHTMLText(String html)
    {
        WebView webView = (WebView) findViewById(R.id.wv_contents);
        if( webView == null ) return;

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + html, "text/html", "UTF-8", null);
    }
}
