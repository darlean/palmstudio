package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.data.ListViewItem;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonListDialog;
import kr.co.palmstudio.palmstudio.util.CommonToast;
import kr.co.palmstudio.palmstudio.util.ImageUtil;
import kr.co.palmstudio.palmstudio.util.PhotoDialog;
import kr.co.palmstudio.palmstudio.util.SexDialog;
import kr.co.palmstudio.palmstudio.util.Util;

/**
 * Created by Administrator on 2016-01-11.
 */
public class OrderActivity extends Activity implements View.OnClickListener
{
    private AQuery m_aq;
    private CommonListDialog commonListDialog ;
    private CommonToast commonToast ;
    private PhotoDialog photoDialog ;
    private SexDialog sexDialog ;

    private int press_photo_btn_id ;

    ArrayList<ListViewItem> photo_count_list, photo_retouch_list ;

    String selected_photo_type = "", selected_photo_retouch = "" ;
    int selected_photo_count = 0 ; //, selected_photo_bg = 1 ;
    String selected_cloth = "" ;
    String selected_photo_bg = "" ;
    String tmp_photo_file_path, photo_file_path_1, photo_file_path_2, photo_file_path_3 ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        m_aq = new AQuery(this);
        commonListDialog = new CommonListDialog(this);
        commonToast = new CommonToast(getApplicationContext()) ;

        photoDialog = new PhotoDialog(this) ;
        photoDialog.findViewById(R.id.btn_take_photo).setOnClickListener(this);
        photoDialog.findViewById(R.id.btn_select_photo).setOnClickListener(this);

        sexDialog = new SexDialog(this) ;
        sexDialog.findViewById(R.id.btn_none).setOnClickListener(this);
        sexDialog.findViewById(R.id.btn_male).setOnClickListener(this);
        sexDialog.findViewById(R.id.btn_female).setOnClickListener(this);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText("주문옵션") ;

        photo_count_list = new ArrayList<ListViewItem>();
        photo_retouch_list = new ArrayList<ListViewItem>();

        String[] arr_photo_count_list = new String[]{ "기본매수", "기본매수 x 2(+1,000원)", "기본매수 x 3(+2,000원)" } ;

        int i = 1 ;
        for ( String str : arr_photo_count_list )
        {
            photo_count_list.add(new ListViewItem(Integer.toString(i), str));
            i++ ;
        }

        String[] arr_photo_retouch_list = new String[]{ "1.선택안함", "2.잡티 제거, 잔머리 수정", "3.얼굴 윤각 보정" } ;

        i = 0 ;
        for ( String str : arr_photo_retouch_list )
        {
            photo_retouch_list.add(new ListViewItem(Integer.toString(i), str));
            i++ ;
        }

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_photo_count).setOnClickListener(this);
        findViewById(R.id.btn_photo_retouch).setOnClickListener(this);
        findViewById(R.id.btn_select_bg).setOnClickListener(this);
        findViewById(R.id.btn_select_cloth).setOnClickListener(this);
        findViewById(R.id.btn_submit).setOnClickListener(this);
        findViewById(R.id.btn_photo_1).setOnClickListener(this);
        findViewById(R.id.btn_photo_2).setOnClickListener(this);
        findViewById(R.id.btn_photo_3).setOnClickListener(this);

        // set text
        //m_aq.id(R.id.edit_memo).text("추가적인 요청입니다.");

        Member.getOrder().clear();

        processIntent();
    }

    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
        processIntent();
    }

    private void processIntent()
    {
        Intent receivedIntent = getIntent() ;

        if ( receivedIntent == null )
            return ;

        if ( receivedIntent.getExtras().getString("TYPE").equals("photoType") )
        {
            selected_photo_type = receivedIntent.getExtras().getString("StrType") ;

            TextView textView = (TextView) findViewById(R.id.tv_seltype) ;
            textView.setText(selected_photo_type) ;

            Log.d("selected_photo_type", selected_photo_type);

            if ( selected_photo_type.startsWith("주민등록증") || selected_photo_type.startsWith("여권 사진") || selected_photo_type.startsWith("운전면허") || selected_photo_type.startsWith("비자") || selected_photo_type.startsWith("수능접수") )
            {
                findViewById(R.id.layout_select_bg).setVisibility(View.GONE);
            }
        }
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent receivedIntent)
    {
        if (resultCode == RESULT_CANCELED)
            return;

        if (requestCode == C.Preference.ACT_SELECT_BG)
        {
            //selected_photo_bg = receivedIntent.getIntExtra("BG", 1) + 1 ;

            String selected_name = receivedIntent.getExtras().getString("BG");
            int id = Util.getId(selected_name, R.drawable.class);

            ImageButton imageButton = (ImageButton) findViewById(R.id.btn_select_bg) ;
            imageButton.setImageResource(id);

            selected_photo_bg = selected_name;
        }
        else if (requestCode == C.Preference.ACT_SELECT_CLOTH)
        {
            String selected_name = receivedIntent.getExtras().getString("BG");
            int id = Util.getId(selected_name, R.drawable.class);

            ImageButton imageButton = (ImageButton) findViewById(R.id.btn_select_cloth) ;
            imageButton.setImageResource(id);

            selected_cloth = selected_name;
        }
        else if (requestCode == C.Preference.ACT_TAKE_PHOTO || requestCode == C.Preference.ACT_SELECT_PHOTO)
        {
            if ( requestCode == C.Preference.ACT_SELECT_PHOTO )
                tmp_photo_file_path = getImagePath(receivedIntent.getData());

            Bitmap bitmap = ImageUtil.loadFixedSizeBitmap(tmp_photo_file_path, 200, 200) ;
            ImageView imageView = (ImageView) findViewById(press_photo_btn_id);
            imageView.setImageBitmap(bitmap);
            imageView.setTag(true);

            if ( press_photo_btn_id == R.id.btn_photo_1 ) {
                Member.getOrder().setUploadImg( tmp_photo_file_path );
                photo_file_path_1 = tmp_photo_file_path;
            }
            else if ( press_photo_btn_id == R.id.btn_photo_2 ) {
                Member.getOrder().setUploadImg2(tmp_photo_file_path);
                photo_file_path_2 = tmp_photo_file_path;
            }
            else if ( press_photo_btn_id == R.id.btn_photo_3 ) {
                Member.getOrder().setUploadImg3(tmp_photo_file_path);
                photo_file_path_3 = tmp_photo_file_path;
            }

            Log.d("Selected the photo", tmp_photo_file_path);
        }
    }

    @Override
    public void onClick(View view)
    {
        final int btn_id = view.getId() ;

        switch(btn_id)
        {
            case R.id.btn_back: {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
                break ;

            case R.id.btn_photo_count:
            {
                final Button button = (Button) view ;

                AdapterView.OnItemClickListener onItemClickListener = (new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
                    {
                        button.setText(commonListDialog.getList().get(position).getLabel());

                        selected_photo_count = position + 1;
                        Log.d( "selected_photo_count", Integer.toString(selected_photo_count) );

                        commonListDialog.dismiss();
                    }
                });

                commonListDialog.setOnItemClickListener(onItemClickListener);
                commonListDialog.setList(photo_count_list);
                commonListDialog.show();
            }
            break ;

            case R.id.btn_photo_retouch:
                {
                    final Button button = (Button) view ;

                    AdapterView.OnItemClickListener onItemClickListener = (new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View v, int position, long id)
                        {
                            button.setText(commonListDialog.getList().get(position).getLabel());

                            selected_photo_retouch = commonListDialog.getList().get(position).getLabel();
                            commonListDialog.dismiss();
                        }
                    });

                    commonListDialog.setOnItemClickListener(onItemClickListener);
                    commonListDialog.setList(photo_retouch_list);
                    commonListDialog.show();
                }
            break ;

            case R.id.btn_select_bg:
            {
                Intent intent = new Intent(getApplicationContext(), SelectBgActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("TYPE", "BG");
                startActivityForResult(intent, C.Preference.ACT_SELECT_BG);
            }
            break ;

            case R.id.btn_none:
                ImageButton imageButton = (ImageButton) findViewById(R.id.btn_select_cloth) ;
                imageButton.setImageResource(android.R.color.transparent);
                selected_cloth = "" ;
                sexDialog.dismiss();
                break ;

            case R.id.btn_male:
            case R.id.btn_female:
            {
                sexDialog.dismiss();

                boolean bMale = true ;

                if ( btn_id == R.id.btn_female )
                {
                    bMale = false ;
                }

                Intent intent = new Intent(getApplicationContext(), SelectBgActivity.class);
                intent.putExtra("TYPE", "CLOTH");
                intent.putExtra("SEX", bMale);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityForResult(intent, C.Preference.ACT_SELECT_CLOTH);
            }
            break ;

            case R.id.btn_select_cloth:
            {
                sexDialog.show();
            }
            break ;

            case R.id.btn_submit:
            {
                // 사진 업데이트 성공하면 다음으로...

                if ( submit() )
                {
                    Intent intent = new Intent(getApplicationContext(), PayActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                }
            }
            break ;

            case R.id.btn_photo_1:
            case R.id.btn_photo_2:
            case R.id.btn_photo_3:
                press_photo_btn_id = btn_id ;
                photoDialog.show();
                break ;


            case R.id.btn_take_photo:
            {
                photoDialog.dismiss();

                File mediaStorageDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), C.Preference.TEMP_DIR);

                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        // Log.i("", "failed to create directory");
                        return ;
                    }
                }

                // Create a media file name
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

                String FILE_PATH = mediaFile.getAbsolutePath();

                // Set file path into single ton way
                Uri mUri = Uri.parse(FILE_PATH);

                tmp_photo_file_path = mUri.toString();

                MediaScannerConnection.scanFile(this,
                        new String[]{FILE_PATH}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                            }
                        });

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE) ;
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile)) ;
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                if ( intent.resolveActivity(getPackageManager()) != null )
                {
                    startActivityForResult(intent, C.Preference.ACT_TAKE_PHOTO) ;
                }
            }
            break ;

            case R.id.btn_select_photo:
            {
                photoDialog.dismiss();

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, C.Preference.ACT_SELECT_PHOTO);
            }
            break ;
        }
    }

    public boolean submit()
    {
        if( !Member.getInstance().isLogin() )
        {
            AlertDialog.Builder ab = new AlertDialog.Builder( this );
            ab.setMessage( "로그인 후 이용이 가능합니다.\n로그인 하시겠습니까?" );
            ab.setNegativeButton(android.R.string.cancel, null);
            ab.setPositiveButton(android.R.string.ok,  new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                }
            });

            ab.setTitle( "로그인 알림" );
            ab.show();

            return false;
        }

        if( selected_photo_count == 0 )
        {
            commonToast.showToast("사진 장수를 선택해 주세요", Toast.LENGTH_LONG);
            return false ;
        }

        if ( selected_photo_retouch.equals("") )
        {
            commonToast.showToast("보정강도를 선택해 주세요", Toast.LENGTH_LONG);
            return false ;
        }

        if( Member.getOrder().getUploadImg() == null )
        {
            commonToast.showToast("첫번째 사진을 선택해 주세요", Toast.LENGTH_LONG);
            return false ;
        }

        Log.d( "selected_photo_count", Integer.toString(selected_photo_count) );

        Member.getOrder().setOption(selected_photo_type, selected_photo_count, selected_photo_retouch, m_aq.id(R.id.edit_memo).getEditText().getText().toString(), selected_photo_bg, selected_cloth);

        return true ;
    }

    public String getImagePath(Uri data)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);

        return imgPath;
    }
}
