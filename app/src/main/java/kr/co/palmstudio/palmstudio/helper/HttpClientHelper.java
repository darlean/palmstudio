package kr.co.palmstudio.palmstudio.helper;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.protocol.ClientContext;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.protocol.HttpContext;
import kr.co.palmstudio.palmstudio.constant.C;

/**
 * Created by 홍군 on 2016-02-07.
 */
public class HttpClientHelper
{
    // Async Http Client를 Static 으로 선언한다.
    private static AsyncHttpClient client = new AsyncHttpClient();

    // 인스턴스 가져오기
    public static AsyncHttpClient getInstance()
    {
        return HttpClientHelper.client;
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        Log.i("get", getAbsoluteUrl(url));

        client.setUserAgent(System.getProperty("http.agent"));
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void get(String url, AsyncHttpResponseHandler responseHandler)
    {
        Log.i("get", getAbsoluteUrl(url));

        client.setUserAgent(System.getProperty("http.agent"));
        client.get(getAbsoluteUrl(url), responseHandler);
    }


    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        Log.i("post", getAbsoluteUrl(url));

        client.setUserAgent(System.getProperty("http.agent"));
        client.post(getAbsoluteUrl(url), params, responseHandler);

        debugCookies();
    }

    public static void post(String url, AsyncHttpResponseHandler responseHandler)
    {
        Log.i("post", getAbsoluteUrl(url));

        client.setUserAgent(System.getProperty("http.agent"));
        client.post(getAbsoluteUrl(url), responseHandler);

        debugCookies();
    }

    private static String getAbsoluteUrl(String relativeUrl)
    {
        return C.BASE_URL + relativeUrl;
    }

    public static void debugCookies()
    {
        HttpContext httpContext = client.getHttpContext();
        CookieStore cookieStore = (CookieStore) httpContext.getAttribute(ClientContext.COOKIE_STORE);
        if( cookieStore == null ) return;

        List<Cookie> cookies = cookieStore.getCookies();

        if( cookies == null ) return;

        String cookieString = "";
        for (int i = 0; i < cookies.size(); i++)
        {
            Cookie eachCookie = cookies.get(i);
            cookieString += eachCookie.getName() + "=" + eachCookie.getValue() + ";";
        }

        Log.i("Debug Cookies", cookieString);

    }

    public static void debugHeaders(String TAG, Header[] headers)
    {
        if (headers != null)
        {
            Log.d(TAG, "Return Headers:");
            for (Header h : headers)
            {
                String _h = String.format(Locale.US, "%s : %s", h.getName(), h.getValue());
                Log.d(TAG, _h);
            }
        }
    }

    public static void debugThrowable(String TAG, Throwable t)
    {
        if (t != null)
        {
            Log.e(TAG, "AsyncHttpClient returned error", t);
        }
    }

    public static void debugStatusCode(String TAG, int statusCode)
    {
        String msg = String.format(Locale.US, "Return Status Code: %d", statusCode);
        Log.d(TAG, msg);
    }

    public static void debugResponse(String TAG, String response)
    {
        if (response != null) {
            Log.d(TAG, "Response data:");
            Log.d(TAG, response);
        }
    }
}
