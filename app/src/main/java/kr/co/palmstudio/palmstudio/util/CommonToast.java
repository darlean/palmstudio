package kr.co.palmstudio.palmstudio.util;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import kr.co.palmstudio.palmstudio.R;
/**
 * Created by Administrator on 2016-01-14.
 */
public class CommonToast extends Toast
{
    Context mContext;
    ImageUtil imageUtil ;

    public CommonToast(Context context) {
        super(context);
        mContext = context;
    }

    public void showToast(String body, int duration){
        // http://developer.android.com/guide/topics/ui/notifiers/toasts.html
        LayoutInflater inflater;
        View v;
        if(false){
            Activity act = (Activity)mContext;
            inflater = act.getLayoutInflater();
            v = inflater.inflate(R.layout.toast_layout, null);
        }else{  // same
            inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.toast_layout, null);
        }
        TextView text = (TextView) v.findViewById(R.id.text);
        text.setText(body);

        show(this,v,duration);
    }

    private void show(Toast toast, View v, int duration)
    {
        toast.setGravity (Gravity.BOTTOM, 0, (int) imageUtil.convertDpToPixel(70, mContext));
        toast.setDuration(duration);
        toast.setView(v);
        toast.show();
    }
}
