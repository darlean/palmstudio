package kr.co.palmstudio.palmstudio.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.Reference;
import java.util.Iterator;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by 홍군 on 2016-02-08.
 */
public class HttpRequest
{
    private final static int        SESSION_OK          = 0;        // 세션 정상
    private final static int        SESSION_EXPIRED     = 1;        // 세션 만료
    private final static int        SESSION_REMAKED     = 2;        // 세션 재생성 완료

    private final static String     CSRF_TOKEN_NAME     = "csrf_test_name";
    private final static String     CSRF_TOKEN          = "6e6405edf59d00fea9d9feff915fe34e";

    private Reference<Context>      m_wContenxt;
    private String                  m_url               = null;     // 경로

    private String                  m_loading_message   = null;     // 로딩 메세지
    private ProgressDialog          m_progress_dlg      = null;     // 프로그레스 다이얼로그

    private HttpRequestListener     m_requestListener   = null;     // 요청 리스너

    final private JsonHttpResponseHandler handler = new JsonHttpResponseHandler()
    {
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response)
        {
            // called when response HTTP status is "200 OK"
            //Log.i("success", response.toString());
            try
            {
                boolean hasSessionResult = false;
                Iterator<String> json_key = response.keys();
                while (json_key.hasNext())
                {
                    String key = json_key.next();
                    if (key.equals("session_result"))
                    {
                        hasSessionResult = true;
                        switch (response.getInt(key))
                        {
                            case SESSION_OK:
                                Log.d("session", "ok");

                                if(m_requestListener != null)
                                    m_requestListener.response(statusCode, response);

                                break;

                            case SESSION_EXPIRED:
                                Log.d("session", "expired");
                                if(m_requestListener != null)
                                    m_requestListener.session_expired(response);

                                break;

                            case SESSION_REMAKED:
                                Log.d("session", "remaked");

                                if(m_requestListener != null)
                                    m_requestListener.session_remaked(response);

                                break;
                        }
                        break;
                    }
                }

                if( hasSessionResult == false )
                {
                    Log.e("session", "not found session result");
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            if( m_progress_dlg != null )
                m_progress_dlg.dismiss();
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
        {
            // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            HttpClientHelper.debugResponse("HttpRequest", "ErrorCode : " + statusCode);
             try
            {
                if( errorResponse != null ) {
                    if (m_requestListener != null)
                        m_requestListener.response_error(statusCode, errorResponse.toString());
                }
                else
                {
                    if (m_requestListener != null)
                        m_requestListener.response_error(statusCode, "");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            if( m_progress_dlg != null )
                m_progress_dlg.dismiss();
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
        {
            HttpClientHelper.debugResponse("HttpRequest", "ErrorCode : " + statusCode + ", Msg : " + responseString);
            try
            {
                if(m_requestListener != null)
                    m_requestListener.response_error(statusCode, responseString );
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            if( m_progress_dlg != null )
                m_progress_dlg.dismiss();
        }
    };

    private HttpRequest()
    {

    }

    public HttpRequest(HttpRequestListener _request_listener )
    {
        this.m_requestListener       = _request_listener;
    }


    public void post_with_dialog( Context _ctx, String _message,  String _relativeUrl, RequestParams _param )
    {
        Log.d( _relativeUrl, "Params " + _param.toString());

        this.m_progress_dlg = ProgressDialog.show(_ctx, "", _message);

        post(_relativeUrl, _param);
    }

    public void post_with_dialog( Context _ctx, String _message,  String _relativeUrl )
    {
        Log.d( _relativeUrl, "Params " );

        this.m_progress_dlg = ProgressDialog.show(_ctx, "", _message);

        HttpClientHelper.post(_relativeUrl, handler);
    }

    public void post( String _relativeUrl, RequestParams _param)
    {
        Log.d( _relativeUrl, "Params " + _param.toString());

        HttpClientHelper.post(_relativeUrl, _param, handler);
    }

    public void post( String _relativeUrl)
    {
        Log.d(_relativeUrl, "Params " );

        HttpClientHelper.post(_relativeUrl, handler);
    }

    public void get_with_dialog( Context _ctx, String _relativeUrl, String _message, RequestParams _param)
    {
        Log.d(_relativeUrl, "Params " + _param.toString());

        this.m_progress_dlg = ProgressDialog.show(_ctx, "", _message);

        HttpClientHelper.get(_relativeUrl, _param, handler);
    }

    public void get( String _relativeUrl, RequestParams _param)
    {
        Log.d( _relativeUrl, "Params " + _param.toString());

        HttpClientHelper.get(_relativeUrl, _param, handler);
    }

    public void get( String _relativeUrl)
    {
        Log.d( _relativeUrl, "Params " );

        HttpClientHelper.get( _relativeUrl, handler);
    }
}
