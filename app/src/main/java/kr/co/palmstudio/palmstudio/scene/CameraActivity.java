package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.camera.CameraPreviewFragment;
import kr.co.palmstudio.palmstudio.camera.CustomCamera;

/**
 * Created by Administrator on 2016-01-11.
 */
public class CameraActivity extends FragmentActivity implements View.OnClickListener
{
    /**
     * Single ton section
     */
    public static CustomCamera camera = CustomCamera.getInstance();
    //public static int case_camera = Receiver.case_camera_in_group_feed;

    /**
     * String section
     */
    public static int current_orientation = 0;

    /**
     * View section
     */
    public static Camera mCamera;
    public static Camera.CameraInfo mCameraInfo =
            new Camera.CameraInfo();

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance(int camera_id) {
        Camera c = null;
        try {
            c = Camera.open(camera_id); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }

        return c; // returns null if mCamera is unavailable
    }

    public static void releaseCamera() {
        // Should release camera before go to the other page
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_camera);

        /*findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_option).setOnClickListener(this);

        findViewById(R.id.btn_gallery2).setOnClickListener(this);
        findViewById(R.id.btn_shoot).setOnClickListener(this);
        findViewById(R.id.btn_camera_rotation).setOnClickListener(this);*/

        // Should initialize new Camera after released
        mCamera = null;
        initialCamera();

        // Should show Fragment Custom Camera Preview firstly
        getSupportFragmentManager().beginTransaction().replace(
                R.id.fl_custom_camera, CameraPreviewFragment.newInstance())
                .commitAllowingStateLoss();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Should release camera before go to the other page
        // Because it need release for the other camera apps using camera,
        // If it not release, current app will not allow the other app use
        if (CameraPreviewFragment.IS_RECORDING_VIDEO) {
            mCamera.stopPreview();

            // release camera
            releaseCamera();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Re-initialize if camera already released
        /**
         * Photo mode
         */
        if (mCamera == null || CameraPreviewFragment.IS_RECORDING_VIDEO) {
            // Initialize camera again
            initialCamera();

            // Should show Fragment Custom Camera Preview firstly
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fl_custom_camera, CameraPreviewFragment.newInstance())
                    .commitAllowingStateLoss();
        }
    }

    /**
     * Initial methods
     */

    private void initialCamera() {
        int camera_mode = -1;

        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                // Back camera
                CameraPreviewFragment.IS_BACK_CAMERA_OR_FRONT_CAMERA = false;
                CameraPreviewFragment.current_camera_id = CustomCamera.CAMERA_BACK;

                camera_mode = i;
                break;
            } else if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                // Front camera
                CameraPreviewFragment.IS_BACK_CAMERA_OR_FRONT_CAMERA = true;
                CameraPreviewFragment.current_camera_id = CustomCamera.CAMERA_FRONT;

                camera_mode = i;
                break;
            }
        }

        // get Camera instance
        mCamera = getCameraInstance(camera_mode);

        // Set Camera Display Orientation
        CameraPreviewFragment.setCameraDisplayOrientation(this, camera_mode);

        // Get orientation listener
        new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                getCurrentOrientation(orientation);
            }
        }.enable();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Should release camera before go to the other page
        // Because it need release for the other camera apps using camera,
        // If it not release, current app will not allow the other app use
        releaseCamera();
    }

    /**
     * Basic methods
     */

    private int getCurrentOrientation(int orientation) {
        orientation = (orientation + 45) / 90 * 90;
        this.current_orientation = orientation % 360;

        return current_orientation;
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;

            case R.id.btn_shoot:
                //mCamera.autoFocus(this);
                //mCamera.takePicture(null, null, this);
                break ;

            case R.id.btn_camera_rotation:
                //swtichCamera() ;
                break ;
        }
    }


}
