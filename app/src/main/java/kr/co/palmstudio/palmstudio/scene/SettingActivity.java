package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.util.CItemData;
import kr.co.palmstudio.palmstudio.util.ItemDataAdapter;

/**
 * Created by darlean on 2016. 1. 16..
 */
public class SettingActivity extends Activity implements View.OnClickListener
{
    // 리스트뷰 선언
    private ListView listview;

    // 데이터를 연결할 Adapter
    ItemDataAdapter adapter;

    // 데이터를 담을 자료구조
    ArrayList<CItemData> alist;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText(R.string.activity_setting_title) ;

        listview = (ListView) findViewById(R.id.listView);

        alist = new ArrayList<CItemData>();
        adapter = new ItemDataAdapter(this, alist, listview);
        adapter.setOnMyItemClick(onMyItemClick);

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(adapter);

        adapter.add(new CItemData(getApplicationContext(), "푸시 알림 설정", 2, false)) ;
        /*adapter.add(new CItemData(getApplicationContext(), "FAQ", 1, false)) ;
        adapter.add(new CItemData(getApplicationContext(), "1:1문의", 1, false)) ;
        adapter.add(new CItemData(getApplicationContext(), "회사정보", 1, false)) ;
        adapter.add(new CItemData(getApplicationContext(), "서비스 이용약관", 1, false)) ;
        adapter.add(new CItemData(getApplicationContext(), "개인정보 취급방침", 1, false)) ;*/

        findViewById(R.id.btn_back).setOnClickListener(this);

        TextView empty = (TextView) findViewById(R.id.list_empty_tv) ;
        empty.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_back:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                break ;
        }
    }

    private ItemDataAdapter.OnMyItemClick onMyItemClick = new ItemDataAdapter.OnMyItemClick()
    {
        public void OnMyItemClick(int position)
        {
            CItemData cItemData = alist.get(position) ;
            String label = cItemData.getLabel() ;

            if ( label == "푸시 알림 설정" )
            {

            }
            else if ( label == "FAQ" )
            {
                Intent intent = new Intent(getApplicationContext(), FaqActivity.class);
                startActivity(intent);
                finish();
            }
            else if ( label == "1:1문의" )
            {
                Intent intent = new Intent(getApplicationContext(), OneToOneActivity.class);
                startActivity(intent);
                finish();
            }
            else if ( label == "회사정보" )
            {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(intent);
                finish();
            }
            else if ( label == "서비스 이용약관" )
            {
                Intent intent = new Intent(getApplicationContext(), ServicePolicyActivity.class);
                startActivity(intent);
                finish();
            }
            else if ( label == "개인정보 취급방침" )
            {
                Intent intent = new Intent(getApplicationContext(), PrivatePolicyActivity.class);
                startActivity(intent);
                finish();
            }
        }
    } ;
}
