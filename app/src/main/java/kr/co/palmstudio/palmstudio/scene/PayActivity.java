package kr.co.palmstudio.palmstudio.scene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.co.palmstudio.palmstudio.R;
import kr.co.palmstudio.palmstudio.constant.C;
import kr.co.palmstudio.palmstudio.data.ListViewItem;
import kr.co.palmstudio.palmstudio.inicis.AppCallSample;
import kr.co.palmstudio.palmstudio.member.Member;
import kr.co.palmstudio.palmstudio.util.CommonListDialog;
import kr.co.palmstudio.palmstudio.util.CommonToast;

/**
 * Created by Administrator on 2016-01-11.
 */
public class PayActivity extends Activity implements View.OnClickListener
{
    private AQuery m_aq;
    private CommonListDialog commonListDialog ;
    private CommonToast commonToast ;

    JSONObject order_info = null;
    int order_price = 4900;
    int delivery_price = 0;
    int option_price = 0;

    private RadioGroup radio_pay_type ;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        m_aq = new AQuery(this);
        commonListDialog = new CommonListDialog(this);
        commonToast = new CommonToast(getApplicationContext()) ;

        TextView textView = (TextView) findViewById(R.id.tv_title) ;
        textView.setText("결제하기") ;

        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.btn_delivery_type).setOnClickListener(this);
        findViewById(R.id.btn_find_address).setOnClickListener(this);
        findViewById(R.id.btn_submit).setOnClickListener(this);

        option_price = 1000 * ( Member.getOrder().getNumOfPhoto() - 1 );
        order_price += option_price;

        m_aq.id(R.id.tv_kindofphoto).text(Member.getOrder().getSelectedPhotoType());
        radio_pay_type = (RadioGroup) findViewById(R.id.radio_pay_type) ;
        m_aq.id(R.id.tv_order_price).text(Integer.toString(order_price) + "원");
        m_aq.id(R.id.tv_delivery_price).text(Integer.toString(delivery_price) + "원");
        m_aq.id(R.id.tv_total_price).text(Integer.toString(order_price + delivery_price) + "원");

        // set text
        m_aq.id(R.id.edit_orderer).text(Member.getInstance().getName());
        m_aq.id(R.id.edit_hp).text(Member.getInstance().getHP());

        m_aq.id(R.id.edit_zip).text(Member.getInstance().getZip());
        m_aq.id(R.id.edit_addr_1).text(Member.getInstance().getAddr1());
        m_aq.id(R.id.edit_addr_2).text(Member.getInstance().getAddr2());

        order_info = null;
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent receivedIntent)
    {
        if (resultCode == RESULT_CANCELED)
            return;

        if (requestCode == C.Preference.ACT_FIND_ADDRESS)
        {
            m_aq.id(R.id.edit_zip).text(receivedIntent.getStringExtra("ZIPCODE")) ;
            m_aq.id(R.id.edit_addr_1).text(receivedIntent.getStringExtra("ADDR1"));
        }
        else if (requestCode == C.Preference.ACT_INISYS_PAY )
        {
            //submit() ;

            AlertDialog.Builder ab = new AlertDialog.Builder( this );
            ab.setMessage("주문이 완료되었습니다.");
            ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), OrderListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });

            ab.setTitle("주문 완료");
            ab.show();
        }
    }

    public boolean submit()
    {
        Log.i("Join", "submit");

        String orderer = m_aq.id(R.id.edit_orderer).getEditText().getText().toString();
        String hp = m_aq.id(R.id.edit_hp).getEditText().getText().toString();

        String zip = m_aq.id(R.id.edit_zip).getEditText().getText().toString();
        String addr_1 = m_aq.id(R.id.edit_addr_1).getEditText().getText().toString();
        String addr_2 = m_aq.id(R.id.edit_addr_2).getEditText().getText().toString();

        String delivery_type = m_aq.id(R.id.btn_delivery_type).getText().toString() ;

        if ( orderer.equals("") )
        {
            commonToast.showToast("주문자를 입력해 주세요", Toast.LENGTH_LONG);
            return false ;
        }
        else if ( hp.equals("") )
        {
            commonToast.showToast("휴대폰 번호를 입력해 주세요", Toast.LENGTH_LONG);
            return false ;
        }
        else if ( zip.equals("") || addr_1.equals("") || addr_2.equals("") )
        {
            commonToast.showToast("배송지 정보를 입력해 주세요", Toast.LENGTH_LONG);
            return false ;
        }

        Member.getOrder().setPayment(orderer, hp, zip, addr_1, addr_2, delivery_type, order_price, delivery_price, 0);
        Member.getOrder().insertOrder( this, this, "recvOrderResult" );

        return true ;
    }

    public void recvOrderResult(int _statusCode, JSONObject _response)
    {
        Log.i("Order", "Result");

        if(_response != null)
        {
            try
            {
                String reason = _response.getString("reason");
                boolean result = _response.getBoolean("result");

                //successful ajax call, show status code and json content
                //Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                //CommonToast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
                if( result )
                {
                    //order_info = new JSONObject();
                    order_info = _response.getJSONObject("order");
                    /// 결제 시작
                    StartPay();

                    Log.i("Order", _response.toString() );
                }
                else
                {
                    CommonToast toast ;
                    toast = new CommonToast(getApplicationContext()) ;
                    toast.showToast(reason, Toast.LENGTH_LONG);
                }

            } catch (JSONException e) {
                e.printStackTrace();

                //ajax error, show error code
                Toast.makeText(getApplicationContext(), "Error:" + e.toString(), Toast.LENGTH_LONG).show();
            }

        }else{

            //ajax error, show error code
            Toast.makeText(getApplicationContext(), "Error:" + _statusCode, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_delivery_type:
            {
                final Button button = (Button) view ;

                AdapterView.OnItemClickListener onItemClickListener = (new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
                    {
                        button.setText(commonListDialog.getList().get(position).getLabel());
                        commonListDialog.dismiss();

                        if( position == 1 )
                            delivery_price = 2500;
                        else
                            delivery_price = 0;

                        m_aq.id(R.id.tv_order_price).text( Integer.toString(order_price) + "원" );
                        m_aq.id(R.id.tv_delivery_price).text( Integer.toString(delivery_price) + "원" );
                        m_aq.id(R.id.tv_total_price).text( Integer.toString(order_price+delivery_price) + "원" );
                    }
                });

                ArrayList<ListViewItem> list = new ArrayList<ListViewItem>();
                list.add(new ListViewItem("0", "일반 우편(무료)"));
                list.add(new ListViewItem("1", "택배 발송(+2500원)"));

                commonListDialog.setOnItemClickListener(onItemClickListener);
                commonListDialog.setList(list);
                commonListDialog.show();
            }
            break ;

            case R.id.btn_back: {
                Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                intent.putExtra("TYPE", "") ;
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();
            }
            break ;

            case R.id.btn_find_address:
            {
                Intent intent = new Intent(getApplicationContext(), AddressActivity.class);
                intent.putExtra("TYPE", "ORDER");
                startActivityForResult(intent, C.Preference.ACT_FIND_ADDRESS);
            }
            break ;

            case R.id.btn_submit:
            {
                if ( submit() )
                {

                }
            }
            break;
        }
    }

    public void StartPay() throws JSONException {
        String pay_type = "wcard" ;

        if ( radio_pay_type.getCheckedRadioButtonId() == R.id.radio_banking )
        {
            pay_type = "vbank" ;
        }
        else if ( radio_pay_type.getCheckedRadioButtonId() == R.id.radio_mobile )
        {
            pay_type = "mobile" ;
        }

        String orderer = m_aq.id(R.id.edit_orderer).getEditText().getText().toString();
        String hp = m_aq.id(R.id.edit_hp).getEditText().getText().toString();

        String por_no = order_info.getString("por_no");
        String por_id = order_info.getString("por_id");

        Intent intent = new Intent(getApplicationContext(), AppCallSample.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("GOODS", Member.getOrder().getSelectedPhotoType()) ;
        intent.putExtra("ORDER_NO", por_no) ;
        intent.putExtra("ORDER_ID", por_id) ;
        intent.putExtra("AMT", order_price+delivery_price) ;
        intent.putExtra("UNAME", orderer) ;
        intent.putExtra("MNAME", "PalmStudio") ;
        intent.putExtra("MOBILE", hp) ;
        intent.putExtra("PAY_TYPE", pay_type) ;
        startActivityForResult(intent, C.Preference.ACT_INISYS_PAY);
    }
}
